import core.model.Commend;
/**
 * 自定义命令的模版.
 */
public class Masterplate_Commend extends Commend
{
	//控制台中调用方式 类名 方法名
	public String a()
	{
		return "执行a命令";
	}
	
	public byte getPower()
	{
		//填写权限
		return 0;
	}

	public String help()
	{
		//填写帮助信息
		return null;
	}

}
