package ext.commend;

import core.model.Commend;
import ext.util.Tools;
/**
 * 一组测试命令.
 */
public class test extends Commend
{
	/**
	 * 时间测试.
	 * @return 运行结果
	 */
	public static String time()
	{
		return (Tools.Date_Log() + " " + Tools.Time_Log()) + "\r\n";
	}
	/**
	 * 一段话测试.
	 * @return 运行结果
	 */
	public String say()
	{
		return ("如果能看到这段话,说明系统的命令模块没有问题!") + "\r\n";
	}
	public String help()
	{
		return 
		("test      测试命令")  	    + "\r\n" +
		("test say  测试命令库是否正常")  + "\r\n" +
		("test time 测试时间工具是否正常\r\n") ;
	}
	public byte getPower()
	{
		return POWER_EVERYONE;
	}
}
