package ext.dll;

import com.sun.jna.Library;
import com.sun.jna.Native;
/**
 * CPU信息.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public interface CPUinfo extends Library
{
	/**
	 * dll库
	 */
	CPUinfo dll = (CPUinfo)Native.loadLibrary("dll/CPUinfo", CPUinfo.class);
	/**
	 * @return CPU信息
	 */
	public String cpuInfo();
	/**
	 * @return CPU占有率
	 */
	public String cpuRatio();
}
