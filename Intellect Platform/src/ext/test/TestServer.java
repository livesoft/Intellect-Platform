package ext.test;

import core.model.commend.udp;
import core.model.thread.BroadcastThread;
import core.model.thread.NodeSocketThread;
import core.model.thread.ServerSocketThread;
/**
 * 服务的测试.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class TestServer extends Thread
{
	public void run()
	{
		Thread thread = Thread.currentThread();
		if(thread.getName().equals("服务器"))
		{
			new Thread(new BroadcastThread()).start();
			ServerSocketThread sst = new ServerSocketThread();
			
			new Thread(sst).start();
			while(true)
			{
				int n = sst.getThreadCounts();
				System.out.println("客服端个数" + n);
				try
				{
					Thread.sleep(3000);
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}else
		{
			udp u = new udp();
			NodeSocketThread nst = null;
			try
			{
				nst = new NodeSocketThread(u.find().replaceAll("接受的信息:", ""));
			} catch (Exception e1)
			{
				e1.printStackTrace();
			}
			new Thread(nst).start();
			
			while(true)
			{
				try
				{
					System.out.println(thread.getName() + "接受的信息:" + nst.getMessage().toString());
				} catch (Exception e)
				{
					System.out.println(thread.getName() + " 无接受到任意信息!");
				}
				try
				{
					Thread.sleep(2000);
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
//	public static void main(String[] args)
//	{
//		TestServer server = new TestServer();
//		server.setName("服务器");
//		server.start();
//		
//		try
//		{
//			Thread.sleep(1000);
//		} catch (InterruptedException e)
//		{
//			e.printStackTrace();
//		}
//		
//		TestServer client1 = new TestServer();
//		client1.setName("客服端1");
//		client1.start();
//		
////		TestServer client2 = new TestServer();
////		client2.setName("客户端2");
////		client2.start();
//		
//		try
//		{
//			Thread.sleep(2000);
//		} catch (InterruptedException e)
//		{
//			e.printStackTrace();
//		}
//		
//	}
}
