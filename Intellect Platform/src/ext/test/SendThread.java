package ext.test;

import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * 在已知的连接中发送信息.
 */
public class SendThread implements Runnable
{
	/** 传送的信息 */
	private Object object;
	/** 传送的连接 */
	private Socket server;
	
	/**
	 * @param object 发送的对象
	 * @param server 发送的连接
	 */
	public SendThread(Object object, Socket server)
	{
		try
		{
			setObject(object);
			setServer(server);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 设置将要传送的信息.
	 * 
	 * @param object 传送的内容.
	 * @throws Exception 如果信息为null,将产生异常.
	 */
	public void setObject(Object object) throws Exception
	{
		if(object!=null)
		{
			this.object = object;
		}else
		{
			throw new Exception("要传送的对象不能为空!");
		}
	}
	
	/**
	 * 设置将要传送的连接.
	 * 
	 * @param server 传送的连接.
	 * @throws Exception 如果连接为null,或者连接已关闭,将产生异常.
	 */
	public void setServer(Socket server) throws Exception
	{
		if(server==null || server.isClosed())
		{
			throw new Exception("Socket不可用!请检查连接!");
		}else
		{
			this.server = server;
		}
	}
	
	/**
	 * 以线程为基础,进行发送信息. 
	 */
	public void run()
	{
		try
		{
			OutputStream os = server.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(object);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
