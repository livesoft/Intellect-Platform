package ext.util;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import core.standard.StandardClass;
/**
 * 平台中的一个工具.
 */
public class Tools extends StandardClass
{
	/**
	 * 将一个字符值集转成字符串.
	 * @param chars 字符值集
	 * @return 要转换的信息.
	 */
	public static String getString(int chars[])
	{
		String info = "";
		for (int i = 0; i < chars.length; i++)
		{
			info = info + (char)chars[i];
		}
		return info;
	}
	/**
	 * 将一个字符串转成字符值集.
	 * @param info 要转换的信息.
	 * @return 字符值集.
	 */
	public static int[] getChars(String info)
	{
		int chars[] = new int[info.length()];
		for (int i = 0; i < chars.length; i++)
		{
			chars[i] = (int)info.charAt(i);
		}
		return chars;
	}
	/**
	 * 执行某一个方法.
	 * @param clas 要执行的类
	 * @param method 要执行的方法
	 * @return 执行的结果.
	 */
	public static Object runMethod(Class<?> clas,String method)
	{
		try
		{
			Method method1 = clas.getMethod(method);
			return method1.invoke(clas.newInstance());
		} catch (Exception e)
		{
			
		} 
		return null;		
	}
	/**
	 * 获取某一个包中的类.
	 * @param packageName 指定的包.
	 * @return 包中类的集合.
	 */
	public static List<Class<?>> getClass(String packageName)
	{
		List<Class<?>> classList = new ArrayList<Class<?>>();//创建List
		String packageName1 = packageName.replace('.', '\\');
		File file;
		try
		{
			file = new File(ClassLoader.getSystemResource("").toURI());
			File dir = new File(file.getAbsolutePath() + "\\" + packageName1);
			File files[] = dir.listFiles();
			for (int i = 0; i < files.length; i++)
			{
				String fileName = files[i].getName();
				String className = fileName.substring(0, fileName.indexOf('.'));
				classList.add(Class.forName(packageName + "." + className));
			}
		} catch (URISyntaxException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return classList;
	}
	/**
	 * 把一个大小格式化.
	 * 参数为 2000 返回 2.0 KB (2,000 字节)
	 * @param size1 要格式化的大小	 * 
	 * @return 格式化后的结果
	 */
	public static String Size_Log(long size1)
	{
		String size = size1 + "";
		String info = "";
		for (int i = 0; i < size.length(); i++)
		{
			int temp = size.length() - 1 - i;
			info = size.charAt(temp) + info;
			if((temp - 1) % 3 == 0)
			{
				info = "," + info ;
			}
		}
		info = "(" + info + " 字节)";
		
		DecimalFormat df = new DecimalFormat("0.0");
		double sized1 = size1/1024d;
		if(sized1 >= 1)
		{
			double sized2 = sized1/1024;
			if(sized2 >= 1)
			{
				double sized3 = sized2/1024;
				if(sized3 >= 1)
				{
					double sized4 = sized3/1024;
					if(sized4 >= 1)
					{
						info = df.format(sized4) + " TB " + info; 
					}else
					{
						info = df.format(sized3) + " GB " + info; 
					}
				}else
				{
					info = df.format(sized2) + " MB " + info; 
				}
			}else
			{
				info = df.format(sized1) + " KB " + info; 
			}
		}else
		{
			info = size1 + " B " + info; 
		}
		return info;
	}
	/**
	 * 删除指定的文件或目录.
	 * 如果删除目录,会递归删除目录中的全部文件.
	 * @param file 指定的文件或目录.
	 * @return 是否删除成功.
	 */
	public static boolean Delete_File(File file)
	{
		if(!file.isDirectory())
		{
			return file.delete();
		}else
		{
			File files[] = file.listFiles();
			for (int i = 0; i < files.length; i++)
			{
				if(!Delete_File(files[i]))
				{
					return false;
				}
			}
			return true;
		}
	}
	/**
	 * 获取日志的日期.
	 * @return yyyy-MM-dd
	 */
	public static String Date_Log()
	{
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	/**
	 * 获取日志的时间.
	 * @return HH:mm:ss
	 */
	public static String Time_Log()
	{
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(date);
	}
}
