package ext.init;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import core.model.Entity;
import core.standard.StandardClass;

/**
 * 初始化用户.
 */
public final class initInfo extends StandardClass
{
	/**
	 * 将实体集合写到文件中.
	 */
	private static void write(HashSet<Entity> userSet,String type)
	{
		byte buf[] = "请勿修改或删除本文件,因此造成的后果自负!<UTF-8>版权所有<李瑜>".getBytes();
		try
		{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data/" + type + ".data"));
			
			oos.writeInt(buf.length);
			oos.write(buf);
			oos.writeObject(userSet);
			oos.close();
			
			System.out.println("写入成功!");
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	/**
	 * 初始化用户.
	 */
	private static void initUser() 
	{
		HashSet<Entity> userSet = new HashSet<Entity>();
//		User u0 = new User("Kiss", "Kiss", User.Manager);//超级用户
//		User u1 = new User("System", "System", User.Manager);//开放管理员
//		User u2 = new User("Soft", "Soft", User.SOFT);//软件开发人员
//		User u3 = new User("Connection", "Connection", User.COMPUTER);//支点使用的账户
//		System.out.println(userSet.add(u0));
//		System.out.println(userSet.add(u1));
//		System.out.println(userSet.add(u2)); 
//		System.out.println(userSet.add(u3));
		write(userSet,"UserInfo");
	}
	/**
	 * 不允许直接运行.
	 */
	protected static void main(String[] args)
	{
		initUser();
	}
}
