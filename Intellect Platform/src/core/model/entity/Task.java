package core.model.entity;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import core.model.Entity;
/**
 * 任务模型.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class Task extends Entity
{
	private static final long serialVersionUID = 2258049154306258454L;
	/** 存放的是Java文件 */
	public static final byte JAVA_FILE = 0;
	/** 存放的是可执行文件 */
	public static final byte EXECUTE_FILE = 1;
	/** 存放的是Java代码 */
	public static final byte JAVA_CODE = 2;
	/** 存放的是平台命令 */
	public static final byte EXECUTE_COMMEND = 3;
	/** 截至时间 */
	public Date date;
	/** 执行的内容 */
	private String str;
	/** 内容的类型 */
	private int type;

	/**
	 * 根据任务内容,类型构造任务
	 * @param str 任务内容
	 * @param type 任务类型
	 */
	public Task(String str,byte type)
	{
		this.str = str;
		this.type = type;
	}
	
	/**
	 * @return str
	 */
	public String getStr()
	{
		return str;
	}

	/**
	 * @param str 要设置的 str
	 */
	public void setStr(String str)
	{
		this.str = str;
	}

	/**
	 * @return type
	 */
	public int getType()
	{
		return type;
	}

	/**
	 * @param type 要设置的 type
	 */
	public void setType(int type)
	{
		this.type = type;
	}

	public String toString()
	{
		return "任务内容:" + str;
	}
	/**
	 * 执行任务
	 */
	public void execIn()
	{
		try
		{
			Process process = exec();
			IO.readInput(process.getInputStream());
			IO.readInput(process.getErrorStream());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/** 执行任务 
	 * @return 执行任务产生的进程
	 * @throws Exception 产生的异常*/
	public Process exec() throws Exception
	{
		switch (type)
		{
			case JAVA_FILE:
				
				return execJava(new File(str));
				
			case JAVA_CODE:
				
				//将Java代码放到Main文件中执行
				StringBuilder builder = new StringBuilder("public class Main{public static void main(String[] args){");
				builder.append(str + "}}");
				new File("temp").mkdirs();
				
				getFM().writeFile(new File("temp/Main.java"), builder.toString().getBytes());
				return execJava(new File("temp/Main.java"));
				
			case EXECUTE_FILE:
				
				return execFile(new File(str));
			default:
				throw new Exception("任务的类型不正确!");
		}
	}
	/**
	 * 执行Java文件
	 */
	private Process execJava(File file) throws Exception
	{
		try
		{
			File dir = file.getParentFile();
			String name = file.getName().substring(0, file.getName().indexOf("."));
			Runtime.getRuntime().exec("javac " + file.getAbsolutePath()).waitFor();
			return Runtime.getRuntime().exec("java " + name,null,dir.getAbsoluteFile());
		} catch (IOException e)
		{
			throw new Exception("执行Java文件错误!");
		}
	}
	/**
	 * 执行文件
	 */
	private Process execFile(File file)
	{
		try
		{
			return Runtime.getRuntime().exec(file.getAbsolutePath());
		} catch (IOException e)
		{
			getIO().error("执行文件错误!", e);
		}
		return null;
	}
}
