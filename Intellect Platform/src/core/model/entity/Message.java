package core.model.entity;

import java.util.Map;
import core.model.Entity;
/**
 * 信息的模型.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class Message extends Entity
{
	/** 序列号 */
	private static final long serialVersionUID = 9062663583416315630L;
	/** 消息名称 */
	private String name;
	/** 消息详细信息 */
	private String message;
	/** 消息附带的任务 */
	private Task task;
	/** 
	 * 根据map构造消息.
	 * map的key有:name,info,commend
	 * @param map 指定的map
	 */
	public Message(Map<String, String> map)
	{
		this(	map.get("name"),
				map.get("info"),
				new Task(map.get("commend"), Task.EXECUTE_COMMEND));
	}
	
	/**
	 * 构造消息
	 * @param name 指定的名字
	 * @param message 指定的信息
	 */
	public Message(String name, String message)
	{
		this.name = name;
		this.message = message;
	}

	/**
	 * 构造消息
	 * @param name 指定的名字
	 * @param message 指定的信息
	 * @param task 指定的任务
	 */
	public Message(String name, String message, Task task)
	{
		this.name = name;
		this.message = message;
		this.task = task;
	}

	/**
	 * @return name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name 要设置的 name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message 要设置的 message
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return task
	 */
	public Task getTask()
	{
		return task;
	}

	/**
	 * @param task 要设置的 task
	 */
	public void setTask(Task task)
	{
		this.task = task;
	}

	public String toString()
	{
		String info = "姓名:" + name + "\r\n详细内容:" + message;
		if(task == null)
		{
			info = info + "\r\n无任务";
		}else
		{
			info = info + "\r\n" + task.toString();
		}
		return info;
	}
}
