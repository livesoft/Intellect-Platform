package core.model.entity;

import core.model.Entity;

/**
 * 用户模型.
 * 默认匿名用户everyone,无密码.
 * 用户的级别:
 * 1.管理者
 * 2.网格接入者
 * 3.软件开发者
 * 4.匿名用户
 * 
 */
public class User extends Entity
{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 801568211946563866L;

	/** 用户名 */
	private String userName;
	
	/** 用户密码 */	
	private String passWord;
	
	/** 用户级别 */
	private byte level;
	
	/** 下次登录是否必须修改密码 */
	private boolean mustAlter;
	
	/** 默认是everyone用户 */
	public User()
	{
		userName = "everyone";
		passWord = "";
		level = POWER_EVERYONE;
		mustAlter = false;
	}
	
	/** 登录的时候需要用到 
	 * @param userName 用户名
	 * @param passWord 密码
	 */
	public User(String userName, String passWord)
	{
		this.userName = userName.trim();
		this.passWord = passWord.trim();
		level = POWER_EVERYONE;
		mustAlter = false;
	}
	
	//初始化的时候使用,使用之后要注释掉,level不允许直接设置
//	public User(String userName, String passWord, byte level)
//	{
//		this.userName = userName;
//		this.passWord = passWord;
//		this.level = level;
//		mustAlter = true;
//	}

	/**
	 * @return 指定的用户名.
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * 设置用户的用户名.
	 * @param userName 用户名
	 */
	public void setUserName(String userName)
	{
		this.userName = userName.trim();
	}

	/**
	 * @return 指定的密码.
	 */
	public String getPassWord()
	{
		return passWord;
	}

	/**
	 * 设置用户的密码.
	 * @param passWord 密码
	 */
	public void setPassWord(String passWord)
	{
		this.passWord = passWord.trim();
	}
	
	/**
	 * 设置指定用户的级别.
	 * @param level 设置的级别
	 * @param user  有权限的用户
	 * @return 修改后的提示语 
	 */
	public String setLevel(User user,byte level)
	{
		try
		{
			if(user != null && user.getLevel() <= level)
			{
				this.level = level;
				return "权限修改成功!";
			}
		}catch(Exception e)
		{
	
		}catch (Error e) 
		{
			
		}
		return "没有权限修改,修改失败!";
	}
	
	/** 返回用户的权限 
	 * @return 权限*/
	public byte getLevel()
	{
		return level;
	}
	
	/**
	 * @return 该用户是否需要修改密码.
	 */
	public boolean isMustAlter()
	{
		return mustAlter;
	}

	/**
	 * @param mustAlter 下次登录是否强制修改密码.
	 */
	public void setMustAlter(boolean mustAlter)
	{
		this.mustAlter = mustAlter;
	}

	/** 展示用户名 */
	public String toString()
	{
		return userName + " " + level;
	}
	
	/**
	 * 判断是否相等.
	 * @param obj 要判断的对象.
	 * @return true 用户名相同.
	 */
	public boolean equals(Object obj)
	{
		if(this == obj)
		{
			return true;
		}
		if(obj != null && obj.getClass() == User.class)
		{ 
			return ((User)obj).getUserName().equals(userName);
		}
		return false;
	}
	
	/**
	 * 根据用户名返回哈希数.
	 */
	public int hashCode()
	{
		return userName.hashCode();
	}
}
