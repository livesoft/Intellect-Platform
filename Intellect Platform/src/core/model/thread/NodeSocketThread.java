package core.model.thread;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import core.model.StandThread;
import core.model.entity.Message;
import core.model.entity.Task;
/**
 * 节点线程.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class NodeSocketThread extends StandThread
{
	/** 连接 */
	private Socket socket;
	/** 信息列表 */
	private ArrayList<Message> messageLists;
	/** 
	 * 根据"IP地址:端口"形式构造线程 
	 * @param address_port IP地址:端口
	 * @throws Exception 连接异常
	 */
	public NodeSocketThread(String address_port) throws Exception
	{
		state = true;
		address_port = address_port.trim();
		//System.out.println(address_port);
		if(address_port != null && address_port.indexOf(":") != -1)
		{
			String args[] = address_port.split(":");
			
			Socket socket = new Socket(args[0], Integer.parseInt(args[1]));
			messageLists = new ArrayList<>();
			this.socket = socket;
		}else
		{
			throw new Exception("所给信息不正确!" + address_port);
		}
	}
	/** 
	 * 根据socket构造线程 
	 * @param socket 指定的socket
	 */
	public NodeSocketThread(Socket socket)
	{
		state = true;
		messageLists = new ArrayList<>();
		this.socket = socket;
	}
	/**
	 * 获取信息.
	 * @return 获取到的信息
	 */
	public Message getMessage()
	{
		for(Message mes : messageLists)
		{
			Message mess = mes;
			messageLists.remove(mess);
			return mes;
		}
		return null;
	}
	/** 线程运行块 */
	public void run()
	{
		ObjectInputStream ois = null;
		while(state)
		{
			try
			{
				System.out.println("接受信息中...");
				ois = new ObjectInputStream(socket.getInputStream());
				Message message= (Message)ois.readObject();
				messageLists.add(message);
				Task tasks = message.getTask();
				if(tasks != null)
				{
					IO.println("消息中有任务,即可执行!");
					tasks.execIn();
				}
			} catch (Exception e)
			{
				IO.println("连接已断开...");
				state = false;
			}
		}
		if(ois != null)
		{
			try
			{
				ois.close();
			} catch (IOException e)
			{
				nodeSocketThread = null;
			}
		}
	}
	/**
	 * 向服务器写信息.
	 */
	public void writeMessage(Message message)
	{
		if(message != null)
		{
			try
			{
				new ObjectOutputStream(socket.getOutputStream()).writeObject(message);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	/**
	 * 获取服务器的信息.
	 * @return 服务器信息
	 */
	public InetAddress getServer()
	{
		return socket.getInetAddress();
	}
}
