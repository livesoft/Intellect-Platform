package core.model.thread;

import java.io.File;
import core.bin.console.Console;
import core.bin.console.Other;
import ext.util.Tools;
/**
 * 自动化线程
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class InterpreterThread extends Other implements Runnable
{
	private Console console;
	/**
	 * 根据控制台构造自动线程.
	 * @param console 控制台
	 */
	public InterpreterThread(Console console)
	{
		this.console = console;
	}
	public void run()
	{
		//如果存在记录文件,但是为非自动化模式
		if(new File(operFile).exists() && !getProperty("自动执行").equals("true"))
		{
			IO.println("自动化文件存在!是否要启用自动化执行模式!(Y/N)");
			
			//提示用户是否进入自动化
			if(IO.next().toLowerCase().equals("y"))
			{
				
				setProperty("自动执行", "true");
				IO.println("以后是否直接自动进入自动化执行模式!(Y/N)");
				//提示用户是否修改配置文件中的自动化
				if(IO.next().toLowerCase().equals("y"))
				{
					writeProperties();//写配置到文件
				}
			}
			
		}
		//如果配置文件中为自动化模式,进入自动化
		if(getProperty("自动执行").equals("true"))
		{
			IO.println(copyAllright());
			console.auto_commend();//自动化模式
		}else
		{
			//删除上次自动化内容
			File file = new File("data/Oper.data");
			Tools.Delete_File(file);
			console.onLine();//默认模式
		}
	}
}
