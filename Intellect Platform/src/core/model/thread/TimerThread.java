package core.model.thread;

import java.util.Date;
import core.bin.console.Other;
import core.model.commend.log;
/**
 * 计时线程.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class TimerThread extends Other implements Runnable
{
	public void run()
	{
		while(true)
		{
			try
			{
				//如果当前时间距离上次操作时间5分钟,断开连接
				long time = (new Date().getTime() - date.getTime()) / 1000;
				if(time/60/5 > 0)
				{
					log.takeNotes(user, "由于5分钟没有操作,与软件的连接已断开!");
					System.out.println("由于5分钟没有操作,与软件的连接已断开!");
					System.exit(0);
				}
				Thread.sleep(5000);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}	
}
