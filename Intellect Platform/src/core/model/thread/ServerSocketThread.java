package core.model.thread;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import core.model.StandThread;
import core.model.entity.Message;
/**
 * 服务线程.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class ServerSocketThread extends StandThread
{
	private int port;
	
	private ArrayList<Thread> threadLists;
	
	private ArrayList<Socket> socketLists;
	
	/**
	 * 构造服务器线程
	 */
	public ServerSocketThread()
	{
		this.port = PORT_CONNECT;
		state = true;
		threadLists = new ArrayList<>();
		socketLists = new ArrayList<>();
	}
	
	/**
	 * 根据端口构造服务器线程
	 * @param port 指定的端口
	 */
	public ServerSocketThread(int port)
	{
		this();
		this.port = port;
	}
	
	public void run()
	{
		ServerSocket server = null;
		Socket socket = null;
		while(state)
		{
			try
			{
				server = new ServerSocket(port);
			} catch (IOException e1)
			{
				//IO.println("服务器正在监听!");
			}
			
			try
			{
				socket = server.accept();
			} catch (Exception e1)
			{
				//IO.println("正在等待客户!");
			}
			if(socket != null)
			{
				IO.println(socket.getInetAddress() + " 已连接");
				socketLists.add(socket);
				Thread thread = new Thread(new NodeSocketThread(socket));
				threadLists.add(thread);
				thread.start();
			}
		}
		if(server != null)
		{
			try
			{
				server.close();
			} catch (IOException e)
			{
				
			}
		}
	}
	/**
	 * 获取活动的线程个数.
	 * @return 线程个数
	 */
	public int getThreadCounts()
	{
		int i = 0;
		for(Thread thread : threadLists)
		{
			if(thread != null && thread.isAlive())
			{
				i++;
			}
		}
		return i;
	}

	/** 向对应的socket写信息. */
	private void writeMessage(Socket socket,Message message)
	{
		if(message != null)
		{
			try
			{
				new ObjectOutputStream(socket.getOutputStream()).writeObject(message);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	/** 向节点端写信息. */
	public void writeMessage(Message message)
	{
		for (Socket socket : socketLists)
		{
			writeMessage(socket, message);
		}
	}
}
