package core.model.thread;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import core.model.StandThread;
import core.model.entity.Message;
/**
 * 平台中进行广播的线程.
 * 
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class BroadcastThread extends StandThread
{
	/** 广播端口 */
	private int port;
	/** 广播的信息 */
	private String message;
	/** 广播线程的状态 */
	private String status = "未开始广播";
	/** 广播的地址 */
	private String address;
	/** 广播的次数 */
	private int i,n;

	/**
	 * 构造默认的广播线程
	 */
	public BroadcastThread()
	{
		int port = PORT_BROADCAST;
		try
		{
			port = (Integer.parseInt(getProperty("广播端口")));
		}catch(Exception e){}
		
		String IP = "";
		try
		{
			IP = InetAddress.getLocalHost().getHostAddress();
		}catch(Exception e){}
		
		setPort(port);
		setMessage(IP + ":" + PORT_CONNECT);
		address = getProperty("广播地址",ADDRESS_BROADCAST);
	}
	/**
	 * 根据端口,信息构造广播线程
	 * @param port 端口
	 * @param message 广播信息
	 */
	public BroadcastThread(int port, String message)
	{
		setMessage(message);
		setPort(port);
		address = getProperty("广播地址",ADDRESS_BROADCAST);
	}

	public void run()
	{
		long time = 2000;// 广播间隔
		try
		{
			time = Long.parseLong(getProperty("广播间隔"));
		} catch (Exception e)
		{
		}

		n = 0;// 广播次数
		try
		{
			n = Integer.parseInt(getProperty("广播次数"));
		} catch (Exception e)
		{
		}

		i = 0;// 广播次数
		MulticastSocket socket = null;
		do
		{
			//System.out.println("服务器 -- 广播中...");
			byte b[] = message.getBytes();// 获取信息
			try
			{
				
				socket = new MulticastSocket(port);
				socket.setTimeToLive(0);
				socket.joinGroup(InetAddress.getByName(address));
				
			} catch (Exception e)
			{
				status = "<" + address + ">(" + port + ")" + "建立广播失败\r\n";
			}
			try
			{
				DatagramPacket Packet = new DatagramPacket(b, b.length,InetAddress.getByName(address), port);
				socket.send(Packet);
				status = "<" + address + ">(" + port + ")" + "广播成功\r\n信息:" + message + "\r\n";
				//System.out.println("广播");
			} catch (Exception e)
			{
				status = "<" + address + ">(" + port + ")" + "广播失败\r\n";
			}
			try//间隔
			{
				Thread.sleep(time);
			} catch (InterruptedException e){}
		} while (++i != n);// 非正数 会 无限广播

		if (socket != null)
		{
			socket.close();
			status = "<" + address + ">(" + port + ")" + "广播关闭\r\n";
		}
	}
	/**
	 * 广播类只能根据次数进行停止.
	 */
	public void stop()
	{
		i = n - 2;
	}
	/**
	 * @return port
	 */
	public int getPort()
	{
		return port;
	}

	/**
	 * @param port 要设置的 port
	 */
	public void setPort(int port)
	{
		this.port = port;
	}

	/**
	 * @return message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message 要设置的 message
	 */
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * @return status
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * @param status 要设置的 status
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}

	/**
	 * @return address
	 */
	public String getAddress()
	{
		return address;
	}

	/**
	 * @param address 要设置的 address
	 */
	public void setAddress(String address)
	{
		this.address = address;
	}
	@Deprecated
	public void writeMessage(Message message){}
}
