package core.model;

import core.model.entity.User;
import core.standard.StandardClass;
/**
 * 标准命令.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public abstract class Commend extends StandardClass
{
	/** 
	 * 获取命令的权限.
	 * 一个命令默认的权限是本地管理员,所以子类需要根据情况手动设置权限.
	 * @return 返回的是执行权限.
	 * @see User
	 */
	public abstract byte getPower();
	
	/**
	 * 组命令中默认执行的.
	 * @return 帮助信息.
	 */
	public abstract String help();
}
