package core.model;

import core.model.entity.Message;
import core.standard.StandardClass;
/**
 * 平台中标准的线程.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public abstract class StandThread extends StandardClass implements Runnable
{
	/** 线程的运行状态 */
	protected boolean state = true;
	/** 线程的执行方法 */
	public abstract void run();
	/** 线程停止方法 */
	public void stop()
	{
		state = false;
	}
	/** 
	 * 获取线程的状态 
	 * @return 线程的运行状态
	 */
	public boolean getState()
	{
		return state;
	}
	/** 
	 * 向线程中写入message 
	 * @param message 待写的信息
	 */
	public abstract void writeMessage(Message message);
}
