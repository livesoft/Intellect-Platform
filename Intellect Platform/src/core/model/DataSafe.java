package core.model;

import core.standard.StandardInterface;

/**
 * 数据安全.
 * 本接口的实现类一定要满足以下条件:<br>
 * 1.如果数据A加密成为数据B,那么数据B解密必须为数据A.
 * 2.不同的数据加密的结果必须不一致.
 * 
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public interface DataSafe extends StandardInterface
{
	/**
	 * 对数据进行加密.
	 * @param info 要加密的信息.
	 * @return 加密过的信息.
	 */
	public abstract int[] Encrypt(int[] info);
	/**
	 * 对数据进行解密.
	 * @param info 要解密的信息.
	 * @return 解密过的信息.
	 */
	public abstract int[] Decipher(int[] info);
}
