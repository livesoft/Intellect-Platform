package core.model;

import java.io.File;
import core.standard.Config;
import core.standard.StandardInterface;
/**
 * 自定义的类加载器.
 * 为了实现命令的热加载,采用自定义类加载器的方式实现.
 * 
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class SingleClassLoder extends ClassLoader implements StandardInterface
{
	/** 环境变量 */
	private String path;
	
	/** 
	 * 根据指定的环境初始化加载器. 
	 * @param path 指定的环境
	 */
	public SingleClassLoder(String path)
	{
		this.path = path;
	}
	/**
	 * 找到类并且加载.
	 * @param name 类的名字
	 * @return 读取到类的对象
	 */
	public Class<?> findClass(String name) throws ClassNotFoundException
	{
		byte[] data = readClassData(name);
		return this.defineClass(null, data, 0, data.length);
	}
	/**
	 * 读取类的具体内容.
	 * @param name 要读取类的名字
	 * @return 读取到的内容
	 * @throws ClassNotFoundException 类没有找到
	 */
	public byte[] readClassData(String name) throws ClassNotFoundException
	{
		name = name.replaceAll("\\.", "/");
		path = path.replaceAll("\\.", "/");
		File file = Config.getFM().getFile(path + "/" + name + ".class");
		byte bytes[] = Config.getFM().readFile(file);
		if(bytes == null)
		{
			throw new ClassNotFoundException("类文件读取失败!");
		}
		return bytes;
	}
}
