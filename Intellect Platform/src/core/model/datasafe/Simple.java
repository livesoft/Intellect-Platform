package core.model.datasafe;

import core.model.DataSafe;

/**
 * 最简单的数据安全.
 */
public class Simple implements DataSafe
{
	/**
	 * 加密.
	 * @param  info 要加密的内容.
	 * @return 加密过的内容
	 */
	public int[] Encrypt(int[] info)
	{
		int newInfo[] = new int[info.length];
		int j = 0;
		for (int i = info.length - 1; i >= 0 ; i--)
		{
			newInfo[j++] = info[i] + i;
		}
		return newInfo;
	}
	/**
	 * 解密.
	 * @param info 要解密的内容.
	 * @return 解密过的内容
	 */
	public int[] Decipher(int[] info)
	{
		int newInfo[] = new int[info.length];
		int j = 0;
		for (int i = info.length - 1; i >= 0 ; i--)
		{
			int temp = info.length - 1 - i;
			newInfo[j++] = info[i] - temp;
		}
		return newInfo;
	}
}
