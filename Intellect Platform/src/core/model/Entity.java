package core.model;

import java.io.Serializable;
import core.standard.StandardClass;

/**
 * 平台中所有的实体.
 * 凡是需要储存到文件中的,都必须继承本类,并且保存时,需要保存为Entity类型的
 * 目的是以后修改代码后,不会因为序列问题而导致出现序列不一致的异常,
 *
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public abstract class Entity extends StandardClass implements Serializable
{
	private static final long serialVersionUID = -643051374216386342L;
}
