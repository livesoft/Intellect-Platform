package core.model.commend;

import core.model.Commend;
import core.model.StandThread;
import core.model.entity.Message;
import core.model.entity.Task;
/**
 * 关于任务的一些操作.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class task extends Commend
{
	private StandThread thread;
	/**
	 * 构造默认的任务
	 */
	public task()
	{
		if(serverSocketThread != null)
		{
			thread = serverSocketThread;
		}else if(nodeSocketThread != null)
		{
			thread = nodeSocketThread;
		}else
		{
			IO.println("平台未连接,请先使用connect命令进行连接!");
		}
	}
	/**
	 * 构造消息
	 */
	public void message()
	{
		IO.print("请输入任务名称     ->");
		String name = IO.nextLine();
		IO.print("请输入任务详细信息 ->");
		message(name, IO.nextLine());
	}
	/**
	 * 发送消息.
	 * @param name 消息名字
	 * @param mes 消息内容
	 */
	public void message(String name,String mes)
	{
		Task tasks = null;
		IO.println("是否夹带任务?(Y/N)");
		if(IO.next().toLowerCase().equals("y"))
		{
			IO.println("请选择任务的类型:");
			IO.println("0:退出创建任务");
			IO.println("1:执行一段Java代码");
			IO.println("2:执行一个Java文件");
			IO.println("3:执行一个可执行文件");
			switch (IO.next())
			{
				case "1":
					
					 IO.println("请输入一段Java代码(在一行),例如:System.out.println(\"Hello Task I Love This\");");
					 tasks = new Task(IO.next(), Task.JAVA_CODE);
					 break;
				case "2":
					 IO.println("请输入一个文件地址!");
					 tasks = new Task(IO.next(), Task.JAVA_FILE);
					 break;
				case "3":
					 IO.println("请输入一个(目标机)可执行文件地址!\r\n或者环境变量已配置的.");
					 tasks = new Task(IO.next(), Task.EXECUTE_FILE);
			}
		}
		Message message;
		if(tasks == null)
		{
			message = new Message(name, mes);
		}else
		{
			message = new Message(name, mes, tasks);
		}
		thread.writeMessage(message);
	}
	
	public byte getPower()
	{
		return POWER_SOFTWARE;
	}

	public String help()
	{
		return 
				"task message 发送一个消息(可以夹带任务)\r\n";
	}

}
