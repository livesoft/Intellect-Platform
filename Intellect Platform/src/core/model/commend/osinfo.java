package core.model.commend;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import core.model.Commend;
import ext.dll.CPUinfo;

/** 
 * 操作系统信息相关的操作.
 * 
 * @since JDK 1.7
 * @version 1.0
 */  
public class osinfo extends Commend
{  
	/** 可使用内存. */  
	private static long JVM_totalMemory;  
	/** 剩余内存. */  
	private static long JVM_freeMemory;  
	/** 剩余的物理内存. */  
	private static long OS_freeMemory; 
	/** 最大可使用内存. */  
	private static long JVM_maxMemory;  
	/** JRE版本 */
	private static String JRE_version;
	/** 操作系统名字. */  
	private static String OS_Name;  
	/** 操作系统架构. */
	private static String OS_aech;
	/** 总的物理内存. */  
	private static long OS_totalMemory;  
	/** 1B = 1B */
	public static final int  B  = 1;
	/** 1KB = 1024B */
	public static final int  KB = 1024;
	/** 1MB = 1048576B */
	public static final int  MB = 1048576;
	/** 1GB = 1073741824B */
	public static final int  GB = 1073741824;
	/** 1TB = 1099511627776B */
	public static final long TB = 1099511627776l;
	/**
	 * 初始化系统信息.
	 */
	static
	{
		setInfo();
	}
	/** 获取系统信息,单位为MB.
	 * @return 运行结果*/
	public static String getinfo()
	{
		return getinfo("MB");
	}
	/**
	 * 根据指定的单位获取系统信息 
	 * @param Measure 单位,可取B,K,M,G,T
	 * @return 运行结果
	 */
	public static String getinfo(String Measure)
	{
		return getinfo_(Measure);
	}
	/** 获取具体的信息内容 */
	private static String getinfo_(String Measure)
	{
		setInfo();
		long temp = MB;
		String args = Measure.split(" ")[0];
		switch (args.charAt(0))
		{
			case 'B':
				temp = B;
				break;
			case 'K':
				temp = KB;
				break;
			case 'G':
				temp = GB;
				break;
			case 'T':
				temp = TB;
				break;
		}
		args = " " + args;
		String info = "操作系统 = "           + OS_Name;
		info = info + "\r\n系统架构 = "       + OS_aech;
		info = info + "\r\n运行版本 = "       + JRE_version;
		info = info + "\r\n系统总共内存 = "   + OS_totalMemory  / temp + args;
		info = info + "\r\n系统空闲内存 = "   + OS_freeMemory   / temp + args;
		info = info + "\r\n虚拟机使用内存 = " + JVM_totalMemory / temp + args;
		info = info + "\r\n虚拟机空闲内存 = " + JVM_freeMemory  / temp + args;
		info = info + "\r\n虚拟机最大内存 = " + JVM_maxMemory   / temp + args;
		return info + "\r\n";
	}
	/** 设置系统信息 */
	private static void setInfo()
	{
		Properties systemProperties = System.getProperties();
		//OperatingSystemMXBean os = ManagementFactory.getOperatingSystemMXBean();
		OS_Name         = systemProperties.getProperty("os.name");
		OS_aech         = systemProperties.getProperty("os.arch");
		JRE_version     = systemProperties.getProperty("java.version");
		JVM_totalMemory = Runtime.getRuntime().totalMemory();
		JVM_freeMemory  = Runtime.getRuntime().freeMemory();
		JVM_maxMemory   = Runtime.getRuntime().maxMemory();
	}
	/** 
	 * 获取虚拟机已用内存.
	 * @return 按B为单位的内存
	 */
	public static long getJVM_totalMemory()
	{
		return JVM_totalMemory;
	}
	/** 
	 * 获取虚拟机可用的内存.
	 * @return 按B为单位的内存
	 */
	public static long getJVM_freeMemory()
	{
		return JVM_freeMemory;
	}
	/** 
	 * 获取系统可用的内存.
	 * @return 按B为单位的内存
	 */
	public static long getOS_freeMemory()
	{
		return OS_freeMemory;
	}
	/** 
	 * 获取虚拟机最多可使用的内存.
	 * @return 按B为单位的内存
	 */
	public static long getJVM_maxMemory()
	{
		return JVM_maxMemory;
	}
	/** 
	 * 获取JRE的版本.
	 * @return JRE的版本.
	 */
	public static String getJRE_version()
	{
		return JRE_version;
	}
	/** 
	 * 获取当前操作系统的名字.
	 * @return 系统名字
	 */
	public static String getOS_Name()
	{
		return OS_Name;
	}
	/** 
	 * 获取当前操作系统的架构.
	 * @return 架构.
	 */
	public static String getOS_aech()
	{
		return OS_aech;
	}
	/** 
	 * 获取系统总共的内存.
	 * @return 按B为单位的内存 
	 */
	public static long getOS_totalMemory()
	{
		return OS_totalMemory;
	}
	/** 保存系统信息,单位为MB. */
	public static void save()
	{
		save("MB");
	}
	/**
	 * 根据指定的单位保存系统信息.
	 * @param Measure 单位.
	 */
	public static void save(String Measure)
	{
		try
		{
			FileWriter fw = new FileWriter("Data/OSInfo.data");
			fw.write(getinfo_(Measure) + "\r\n" + cpuinfo_());
			fw.close();
			System.out.println("保存系统信息成功!");
			return;
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		System.out.println("保存系统信息失败!");
	}
	/** 获取CPU信息. 
	 * @return 运行结果*/
	public static String cpuinfo()
	{
		return cpuinfo_();
	}
	/** 从dll中获取CPU信息 */
	private static String cpuinfo_()
	{
		if(OS_Name.toLowerCase().startsWith("windows"))//windows平台调用dll
		{
			return CPUinfo.dll.cpuInfo();
		}else
		{
			return "该平台暂时不支持!";
		}
	}
	/** 获取CPU的占有率 
	 * @return 运行结果*/
	public static String cpuratio()
	{
		if(OS_Name.toLowerCase().startsWith("windows"))//windows平台调用dll
		{
			return CPUinfo.dll.cpuRatio();
		}else
		{
			return "该平台暂时不支持!";
		}
	}
	/** 开发人员命令 */
	public byte getPower()
	{
		return POWER_SOFTWARE;
	}
	/** osinfo 帮助文档 */
	public String help()
	{
		return 
		("osinfo getinfo [BKMGT] 获取当前系统的信息,默认为MB") 	 + "\r\n" +
		("osinfo cpuinfo         获取当前CPU的信息")		   	 + "\r\n" +
		("osinfo cpuratio        获取当前CPU的占有率")		 + "\r\n" +
		("osinfo save    [BKMGT] 把当前的系统,CPU信息保存到文件中,默认为MB\r\n");
	}
}  