package core.model.commend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import core.model.Commend;
import core.model.entity.User;
import ext.util.Tools;
/**
 * 关于日志的一些操作.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class log extends Commend
{
	/** 当天的日志文件 */
	private static File logFile;
	static
	{
		//初始化当天日志文件
		String path = new File("").getAbsolutePath();
		File dirs = new File(path + getProperty("日志文件"));
		dirs.mkdirs();
		logFile = new File(dirs.getAbsolutePath() + "/" + Tools.Date_Log() + ".log");
		if(!logFile.exists())
		{
			try
			{
				logFile.createNewFile();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
	/**
	 * 在当天的日志文件中建立一条记录.
	 * @param user    记录操作的用户
	 * @param commend 记录操作的命令
	 */
	public static void takeNotes(User user,String commend)
	{
		String info = "[ " + user.getUserName() + " " + Tools.Time_Log() + " ]" + " " + commend + "\r\n"; 
		try
		{
			FileWriter fw = new FileWriter(logFile,true);
			fw.write(info);
			fw.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 删除全部的日志文件.
	 * @return 运行结果
	 */
	public static String delete()
	{
		File dir = logFile.getParentFile();
		if(Tools.Delete_File(dir))
		{
			dir.mkdirs();
			log.takeNotes(user, "删除日志文件夹!");
			return "日志文件夹清空!\r\n";
		}
		return "日志文件夹删除失败!\r\n";		
	}
	/**
	 * 删除指定序号的日志文件.
	 * 序号可以通过 log list 命令查看.
	 * @param args 指定的序号.
	 * @return 运行结果
	 */
	public static String delete(String args)
	{
		String num = args.split(" ")[0];
		File dir = logFile.getParentFile();
		File files[] = dir.listFiles();
		for (int i = 0; i < files.length; i++)
		{
			if(num.equals(i + ""))
			{
				Tools.Delete_File(files[i]);
				return num + " 文件删除成功!";
			}
		}
		return num + " 没有找到,删除失败!";
	}
	/**
	 * 显示当前日志的总容量.
	 * @return 运行结果
	 */
	public static String size()
	{
		File dir = logFile.getParentFile();
		File files[] = dir.listFiles();
		long size = 0;
		for (int i = 0; i < files.length; i++)
		{
			size = size + files[i].length();
		}
		return ("日志文件总个数: " + files.length + " 个") + "\r\n" +
		 	   ("日志占据总容量: " + Tools.Size_Log(size) + "\r\n");
	}
	/**
	 * 显示当前日志的详细信息.
	 * @return 运行结果
	 */
	public static String list()
	{
		String info = "";
		File dir = logFile.getParentFile();
		File files[] = dir.listFiles();
		for (int i = 0; i < files.length; i++)
		{
			info = info + ((i + " " + files[i].getName() + " " + files[i].length() + " B")) + "\r\n";
		}
		return info;
	}
	/**
	 * 显示当天日志的内容.
	 * @return 运行结果
	 */
	public static String disp()
	{
		String info = "查看日志失败!";
		try
		{
			Scanner sc = new Scanner(logFile);
			while(sc.hasNext())
			{
				info = info + sc.nextLine() + "\r\n";
			}
			sc.close();
		} catch (FileNotFoundException e)
		{
		}
		return info;
	} 
	/** 管理员命令. */
	public byte getPower()
	{
		return POWER_MANAGER;
	}
	/** log的帮助信息. */
	public String help()
	{
		return 
		("log delete [num] 删除全部[指定]日志.") + "\r\n" + 
		("log size         显示日志的总容量")	    + "\r\n" + 
		("log disp         显示当天的日志内容")  	+ "\r\n" + 
		("log list         显示日志的详细信息\r\n");
	}
}
