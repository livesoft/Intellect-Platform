package core.model.commend;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import core.model.Commend;
import core.model.thread.BroadcastThread;
/**
 * 关于广播的一些操作.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class udp extends Commend
{
	/**
	 * 建立默认的广播
	 */
	public void build()
	{
		broadcastThread = new BroadcastThread();
		new Thread(broadcastThread).start();
	}
	/**
	 * 建立广播.
	 * @param port 广播的端口
	 */
	public void build(int port)
	{
		
		broadcastThread = new BroadcastThread();
		broadcastThread.setPort(port);
		new Thread(broadcastThread).start();
	}
	/**
	 * 根据配置查找广播.
	 * @return 运行结果
	 */
	public String find()
	{
		try
		{
			return find(getProperty("广播地址"),Integer.parseInt(getProperty("广播端口")));
		}catch(Exception e)
		{
			return find(getProperty("广播地址"),7039);
		}
	}
	/**
	 * 根据指定地址,端口查找广播.
	 * @param address 查找地址组
	 * @param port 查找的端口
	 * @return 运行结果
	 */
	public String find(String address,int port)
	{
		try
		{
			byte[] buf = new byte[2048];
			MulticastSocket socket = new MulticastSocket(port);
			socket.joinGroup(InetAddress.getByName(address));
			DatagramPacket packet = new DatagramPacket(buf, buf.length);  
			//System.out.println(packet.getAddress() + " " + packet.getPort() + " 接收信息");
			socket.receive(packet);
			socket.close();
			return new String(buf);
		}catch (Exception e){e.printStackTrace();}
		return "接收失败\r\n";
	}
	/**
	 * 停止广播.
	 */
	public void stop()
	{
		broadcastThread.stop();
	}
	/**
	 * 查看当前广播的状态.
	 * @return 运行结果
	 */
	public String state()
	{
		if(broadcastThread == null)
		{
			return "广播线程未成功建立!\r\n";
		}
		return broadcastThread.getStatus();
	}
	/** 任何人都可以执行 */
	public byte getPower()
	{
		return POWER_EVERYONE;
	}
	/** 命令说明 */
	public String help()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("udp build [port]   创建一个[指定端口]接收命令的节点\r\n");
		stringBuilder.append("udp state          查看广播状态\r\n");
		stringBuilder.append("udp stop           停止广播\r\n");
		stringBuilder.append("udp find[IP,port]  在默认[指定]配置找广播的内容\r\n");
		return stringBuilder.toString();
	}

}
