package core.model.commend;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import javax.swing.JOptionPane;
import core.model.Commend;
import ext.util.Tools;
/**
 * 关于自动化的一些操作.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class remeber extends Commend
{
	/** 当前用户信息所在的表. */
	private static String userFile = "data/User.data";
	/** 取消记住当前用户. 
	 * @return 运行结果*/
	public static String unuser()
	{
		boolean is = Tools.Delete_File(new File(userFile));
		if(is)
		{
			return "删除用户历史记录成功!\r\n";
		}else
		{
			return "删除用户历史记录失败!\r\n";
		}
	}
	//todo 图形化需要再做一个
	/** 在本机中记录当前用户. */
	public static void user()
	{ 
		if(mangerModel == MODE_GUI)
		{
			if(JOptionPane.showConfirmDialog(null, "你确定记住本用户吗?本用户将加密存储到硬盘中!") == 0)
			{
				remeberUser();
			}
		}else
		{
			System.out.println("你确定记住本用户吗?本用户将加密存储到硬盘中!(Y/N)");
			
			String info = IO.next();
			if(info.toLowerCase().equals("y"))
			{
				remeberUser();
			}
			
		}
	}
	/** 在硬盘中加密存储当前用户. */
	private static void remeberUser()
	{
		int chars[] = Tools.getChars(user.getUserName() + " " + user.getPassWord());//密码的字符表示形式
		try
		{
			//加密储存到文件中
			Class<?> clas = Class.forName(getProperty("加密方式"));
			Method method = clas.getMethod("Encrypt",int[].class);//获取加密方法
			int newChars[] = (int[]) method.invoke(clas.newInstance(), chars);//执行加密
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(userFile));
			oos.writeObject(newChars);
			oos.close();
			System.out.println("信息保存成功!");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	/** 记录本次操作的命令. 
	 * @return 运行结果*/
	public static String oper()
	{
		remeberOper = true;
		return "已开启命令记录模式,仅本次命令有效!\r\n";
	}
	/** 软件开发使用的命令. */
	public byte getPower()
	{
		return POWER_NETER;
	}
	public String help()
	{
		return
		("remeber user   在本机中存储用户的信息!")  	 + "\r\n" +
		("remeber unuser 不在本机中存储用户的信息!") 	 + "\r\n" +
		("remeber oper   在本机中存储即将要操作的记录!\r\n");
	}
}
