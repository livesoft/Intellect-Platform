package core.model.commend;

import core.model.Commend;
//todo
/**
 * 关于错误的一些操作.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class error extends Commend
{

	public byte getPower()
	{
		return 0;
	}

	public String help()
	{
		return
		("显示关于操作出错的详细信息!\r\n");
	}
}
