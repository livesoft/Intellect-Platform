package core.model.commend;

import java.io.IOException;
import core.model.Commend;
/**
 * pc端一些控制.
 * 案例1.手机远程控制电脑
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class pc extends Commend
{
	/**
	 * 执行某一个命令
	 * @param path 指定的命令
	 */
	public void exec(String path)
	{
		try
		{
			Runtime.getRuntime().exec(path);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 打开QQ
	 */
	public void qq()
	{
		exec("D:\\program files (x86)\\Tencent\\QQ\\Bin\\QQ.exe");//写自己电脑的绝对路径
	}
	
	//还可以扩展其他方法
	public byte getPower()
	{
		return POWER_EVERYONE;
	}

	public String help()
	{
		return null;
	}
}
