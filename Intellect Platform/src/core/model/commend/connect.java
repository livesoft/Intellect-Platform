package core.model.commend;

import core.model.Commend;
import core.model.thread.NodeSocketThread;
import core.model.thread.ServerSocketThread;
/**
 * 关于连接的一些操作.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class connect extends Commend
{
	/**
	 * 将本次建立为服务器端.
	 */
	public void server()
	{
		if(serverSocketThread == null && nodeSocketThread == null)
		{
			serverSocketThread = new ServerSocketThread();
			new Thread(serverSocketThread).start();	
		}
		IO.println("已有线程启动,无法新建线程!可以使用state查看.");
	}
	/**
	 * 将本次建立为节点端.
	 */
	public void client()
	{
		if(serverSocketThread == null && nodeSocketThread == null)
		{
			try
			{
				nodeSocketThread = new NodeSocketThread(new udp().find());
			} catch (Exception e)
			{
				IO.error("节点连接建立失败!", e);
			}
			new Thread(nodeSocketThread).start();
		}
		IO.println("已有线程启动,无法新建线程!可以使用state查看.");
	}
	/**
	 * 将本次建立为节点端.
	 * @param address_port 指定的地址:端口号
	 */
	public void client(String address_port)
	{
		if(serverSocketThread == null && nodeSocketThread == null)
		{
			try
			{
				nodeSocketThread = new NodeSocketThread(address_port);
			} catch (Exception e)
			{
				IO.error("节点连接建立失败!", e);
			}
			new Thread(nodeSocketThread).start();
		}
		IO.println("已有线程启动,无法新建线程!可以使用state查看.");
	}
	/**
	 * 将线程停止.
	 */
	public void stop()
	{
		if(serverSocketThread != null)
		{
			serverSocketThread.stop();
			serverSocketThread = null;
		}else if(nodeSocketThread != null)
		{
			nodeSocketThread.stop();
			nodeSocketThread = null;
		}
	}
	/**
	 * 查看当前线程的状态
	 * @return 线程的状态
	 */
	public String state()
	{
		if(serverSocketThread != null)
		{
			return "服务线程" + (serverSocketThread.getState() ? "运行中" : "已停止") + "\r\n"; 
		}else if(nodeSocketThread != null)
		{
			return "节点线程" + (nodeSocketThread.getState() ? "运行中" : "已停止") + "\r\n"; 
		}else
		{
			return "未启动任何线程!\r\n";
		}
	}
	/**
	 * 获取一些信息.
	 */
	public void get()
	{
		String num = "";
		do
		{
			IO.println("请选择想要获取的信息:");
			IO.println("0.退出获取信息");	
			IO.println("1.服务器连接节点个数");
			IO.println("2.节点连接的服务器");
			num = IO.next();
			
			if(num.equals("1"))
			{
				if(serverSocketThread != null)
				{
					IO.println("节点个数:" + serverSocketThread.getThreadCounts() + "\r\n");
				}else
				{
					IO.println("服务器未开启!\r\n");
				}
			}else if (num.equals("2"))
			{
				if(nodeSocketThread != null)
				{
					IO.println("服务器信息:" + nodeSocketThread.getServer() + "\r\n");
				}else
				{
					IO.println("节点未开启!\r\n");
				}
			}
			
		}while(!num.equals("0"));
	}
	/** 任何人都可以建立连接 */
	public byte getPower()
	{
		return POWER_EVERYONE;
	}
	/** 命令说明 */
	public String help()
	{
		return 
			   "每次只能启动服务线程或节点线程,如果想切换线程,必须先stop.\r\n" +
			   "connect server               将本次建立为服务器端.\r\n" + 
	           "connect client[address:port] 将本次[连接指定的地址]建立为节点端.\r\n" +
			   "connect state                查看线程的状态.\r\n" +
	           "connect get                  后去一些关于线程的信息\r\n" +
	           "connect stop                 停止当前的线程.\r\n";
	}

}
