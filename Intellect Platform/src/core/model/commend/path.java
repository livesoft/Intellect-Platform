package core.model.commend;

import core.model.Commend;
import core.model.SingleClassLoder;
/**
 * 关于命令路径的一些操作.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class path extends Commend
{
	private static String path = getProperty("命令路径");
	/**
	 * 根据命令路径找命令.
	 * @param commend 欲寻找的命令
	 * @return 命令所在的类,如果找不到命令,返回null 
	 * @throws Exception 加载器异常
	 * @throws ClassNotFoundException 在环境中找不到命令
	 */
	public static Class<?> findCommend(String commend) throws Exception 
	{
		String paths[] = path.split(";");
		for (int i = 0; i < paths.length; i++)
		{
			Class<?> commendClass = null;
			try
			{
				SingleClassLoder classLoader = new SingleClassLoder(paths[i]);//第i个命令路径下的命令
				commendClass = classLoader.findClass(commend);//加载类
			}catch(Exception e){}
			
			if(commendClass != null)
			{
				if(Commend.class.isAssignableFrom(commendClass))
				{
					return commendClass;
				}else
				{
					throw new Exception("命令不是合法的!命令必须继承" + Commend.class + "类");
				}
			}
		}
		throw new ClassNotFoundException(commend + "在环境中没有找到!");
	}
	/**
	 * 显示当前命令路径
	 * @return 运行结果
	 */
	public static String disp()
	{
		return "当前命令路径:" + path + "\r\n";
	}
	public byte getPower()
	{
		return POWER_SOFTWARE;
	}
	
	public String help()
	{
		return "path disp 显示当前命令路径\r\n";
	}
//	public static void main(String[] args)
//	{
//		try
//		{
//			Class<?> cla = findCommend("path");
//			System.out.println(cla.getClass().toString());
//		} catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
}
