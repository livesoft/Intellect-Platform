package core.model.commend;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JOptionPane;
import core.model.Commend;
import core.model.Entity;
import core.model.entity.User;

/**
 * 用户相关的操作.
 * 
 * @since JDK 1.6
 * @version 1.0
 */

//todo 对于图形化窗体 需要重新写一些 注册 登陆 修改密码的方法
@SuppressWarnings("unchecked")
public class operuser extends Commend
{
	private static File userInfo;
	/** 将用户存在HashSet上 */
	private static HashSet<Entity> userSet;
	static
	{
		//用户信息所在文件
		userInfo = new File("data/UserInfo.data");
		if(userInfo.exists())
		{
			try
			{
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(userInfo));
				byte buf[] = new byte[ois.readInt()];//读取版权
				ois.read(buf, 0, buf.length);
				if(new String(buf).endsWith("<李瑜>"))
				{
					userSet = (HashSet<Entity>) ois.readObject();//版权正确
				}else
				{
					System.out.println("用户信息表已经非法修改!");
					userSet = new HashSet<Entity>();
				}
				ois.close();
			} catch (Exception e)
			{
				System.out.println("用户信息载入失败!错误信息:" + e.getMessage());
				userSet = new HashSet<Entity>();
			}
		}else
		{
			System.out.println("用户信息载入失败!错误:信息文件不存在!");
			userSet = new HashSet<Entity>();
		}
	}
	/** 注册用户的缩写 */
	public static void su()
	{
		saveuser();
	}
	/**
	 * 注册一个新的用户.<br>
	 * 注册的用户权限和当前登录用户的权限一致.
	 */
	public static void saveuser()
	{
		if(mangerModel == MODE_GUI)
		{
			
		}else
		{
			System.out.println("请输入新用户的用户名:");
			String userName = IO.next().trim();
			String passWord = inputPassWord();
			if(userName.length() > 0 && passWord.length() > 0)
			{
				User newUser = new User(userName, passWord);
				newUser.setLevel(user,user.getLevel());
				if(saveUser(newUser))
				{
					System.out.println("新用户注册成功!");
				}else
				{
					System.out.println("用户注册失败!用户已存在!");
				}
			}
			System.out.println("请输入正确的用户名或密码!(前后不能有空格)");		
			
		}
	}
	/** 将用户信息保存到set中 */
	private static boolean saveUser(User user)
	{
		boolean b = userSet.add(user);
		saveToFile();
		return b;
	}
	/** 删除用户的缩写 */
	public static void du()
	{
		deleteUser();
	}
	/** 
	 * 删除当前登录的用户.<br>
	 * 除了默认匿名用户everyone,其他用户都可以删除.<br>
	 * 删除管理员时,要特别的注意.
	 */
	public static void deleteUser()
	{
		if(mangerModel == MODE_GUI)
		{
			if(user.getUserName().equals("everyone"))
			{
				JOptionPane.showMessageDialog(null, "该用户不能删除!");
			}else
			{
				String code = (int)(Math.random()*100) + "" + (int)(Math.random()*100) + "" + (int)(Math.random()*100);
				if(JOptionPane.showInputDialog("请输入验证码: " + code).equals(code))
				{
					if(deleteUser(user))
					{
						JOptionPane.showMessageDialog(null, "用户删除成功!");
						user = new User();
					}else
					{
						JOptionPane.showMessageDialog(null, "用户删除失败!");
					}
				}
				JOptionPane.showMessageDialog(null, "输入的验证码错误!");
			}
		}else
		{
			if(user.getUserName().equals("everyone"))
			{
				System.out.println("该用户不能删除!");
			}else
			{
				
				String code = (int)(Math.random()*100) + "" + (int)(Math.random()*100) + "" + (int)(Math.random()*100);
				System.out.println("请输入验证码: " + code);
				if(IO.next().equals(code))
				{
					if(deleteUser(user))
					{
						System.out.println("用户删除成功!");
						user = new User();
					}else
					{
						System.out.println("用户删除失败!");
					}
				}
				System.out.println("输入的验证码错误!");
				
			}
		}
	}
	/** 从集合中删除用户 */
	private static boolean deleteUser(User user)
	{
		boolean b = userSet.remove(user);
		saveToFile();
		return b;
	}
	/**
	 * 更改用户的权限.
	 * @param userNameAndLevel 要设置的用户名和权限
	 */
	public static void setlevel(String userNameAndLevel)
	{
		if(mangerModel == MODE_GUI)
		{
			
		}else
		{
			String args[] = userNameAndLevel.split(" ");
			if(args.length < 2)
			{
				System.out.println("所给的参数不够\r\nsetlevel 用户名 权限");
			}else
			{
				User temp = findUser(args[0]);
				if(temp == null)
				{
					System.out.println("所给的用户名不存在!");
				}else
				{
					try
					{
						byte level = Byte.parseByte(args[1]);
						if(level > 0 && level < 5)
						{
							System.out.println(temp.setLevel(user, level));
						}else
						{
							System.out.println("所给的权限不在范围!");
						}
					}catch(Exception e)
					{
						System.out.println("权限参数出错,只能为:1,2,3,4");
					}
				}
			}	
		}
	}
	/**
	 * 输入2次密码,用户重置或修改.<br>
	 * 特别的,本方法要在 JDK 1.6,且原生控制台中运行.
	 */
	private static String inputPassWord()
	{
		if(mangerModel == MODE_GUI)
		{
			return "";
		}else
		{
			
			String pass = "";
			String repass = " ";
			do
			{
				System.out.print("请输入新的密码:");
				try
				{
					//以下在控制台中使用  暗文
					pass = new String(System.console().readPassword()).trim();
				}catch(Exception e)
				{
					//以下在任何地方使用   明文
					pass = IO.next().trim();
				}
				
				System.out.print("请再次输入新的密码:");
				try
				{
					//以下在控制台中使用  暗文
					repass = new String(System.console().readPassword());
				}catch(Exception e)
				{
					//以下在任何地方使用   明文
					repass = IO.next();
				}
				
				if(!pass.equals(repass))
				{
					System.out.println("两次输入的密码不一致!");
				}
			}while(!pass.equals(repass));
			
			return pass;
		}
	}
	/** 修改密码命令的缩写 */
	public static void cp()
	{
		changepassword();
	}
	/**
	 * 修改当前用户的密码.
	 * @return 运行结果
	 */
	public static String changepassword()
	{
		User temp = findUser(user.getUserName());//获取当前用户
		if(temp != null && !temp.getUserName().equals("everyone"))
		{
			String passWord = inputPassWord();
			changePassword(temp, passWord);
			return "密码修改成功\r\n";
		}else
		{
			return "当前用户不能修改密码!\r\n";
		}
	}
	/**
	 * 在集合中修改用户的密码.
	 */
	private static String changePassword(User user,String newpassword)
	{
		user.setPassWord(newpassword);
		saveToFile();
		return ("密码修改成功!");
	}
	/**
	 * 判断用户是否存在.
	 * @param testuser 指定的用户
	 * @return 该用户的级别
	 * @see User
	 */
	public static byte exists(User testuser)
	{
		//不为空 集合中有这个用户名
		if(testuser != null && !userSet.isEmpty() && userSet.contains(testuser))
		{
			User temp = findUser(testuser.getUserName());
			if(testuser.getPassWord().equals(temp.getPassWord()))
			{
				user = temp;	
				return temp.getLevel();
			}
		}
		return -1; 
	}
	/**
	 * 在集合中找到对象的引用.
	 * @param userName 用户名
	 * @return User 对象的引用,如果没有则为null
	 */
	private static User findUser(String userName)
	{
		Iterator<Entity> iterator = userSet.iterator();
		while(iterator.hasNext())
		{
			User temp = (User)iterator.next();
			if(temp.getUserName().equals(userName))
			{
				return temp;
			}
		}
		return null;
	}
	/** 把集合保存到文件中 */
	private static String saveToFile()
	{
		byte buf[] = "请勿修改或删除本文件,因此造成的后果自负!<UTF-8>版权所有<李瑜>".getBytes();
		try
		{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(userInfo));
			
			oos.writeInt(buf.length);
			oos.write(buf);
			oos.writeObject(userSet);
			oos.close();
			
			return "用户信息 - 保存成功!\r\n";
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return "用户信息 - 保存失败!\r\n";
	}
	/**
	 * 该组命令可以让任何人使用.
	 */
	public byte getPower()
	{
		return POWER_EVERYONE;
	}
	/** 默认的信息 */
	public String help()
	{
		return 
		("operuser saveuser(su)         注册一个新的用户,权限跟当前登录用户权限同级")  	+ "\r\n" +
		("operuser deleteuser(du)       删除当前登录的用户,会有一个验证码确认一下")    	+ "\r\n" +
		("operuser changepassword(cp)   修改当前登录用户的密码")				 	+ "\r\n" +
		("operuser setlevel {用户名 权限}   设置指定用户名的权限,当前登录用户必须比这个权限高\r\n");
	}
}
