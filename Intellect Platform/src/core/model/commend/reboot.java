package core.model.commend;

import java.io.File;
import core.model.Commend;
/**
 * 关于重启的一些操作.
 * @version 1.0
 * @since JDK1.6
 * @author 李瑜
 */
public class reboot extends Commend
{
	//写成命令 方便以后修改
	/**	重启管理平台 
	 * @param arg 参数
	 * @return 运行结果
	 */
	public static String start(String arg)
	{
		try
		{
			Runtime.getRuntime().gc();
			File file = new File(arg);
			if(file.exists())
			{
				Runtime.getRuntime().exec("cmd.exe /c start " + file.getAbsolutePath());
				return "";
			}else
			{
				return "重启失败!执行文件找不到!";
			}
		}catch (Exception e) 
		{
			
		}
		return "重启失败!";
	}
	/** 任何人都可以运行 */
	public byte getPower()
	{
		return POWER_EVERYONE;
	}
	
	public String help()
	{
		return "\r\n";
	}
}
