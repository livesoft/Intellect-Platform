package core.standard;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Method;
import javax.swing.JTextArea;
import core.model.commend.operuser;
import core.model.entity.User;
import core.model.thread.BroadcastThread;
import core.model.thread.NodeSocketThread;
import core.model.thread.ServerSocketThread;
import ext.util.Tools;
/** 
 * 平台的标准.
 * 
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */

public abstract class StandardClass extends Config implements StandardInterface
{
	/** 平台中后台服务Socket */
	protected static ServerSocketThread serverSocketThread;
	
	/** 平台中后台Socket */
	protected static NodeSocketThread nodeSocketThread;
	
	/** 平台中广播 */
	protected static BroadcastThread broadcastThread;

	/** 当前管理平台的模式. */
	protected static byte mangerModel = MODE_COMMEND;
	
	/** 图形化管理窗口的输出口 */
	protected static JTextArea area = new JTextArea();;
	
	/** 全局的用户,指当前登录用户. */
	protected static User user;
	
	/** 全局的信号,是否要记住操作. */
	protected static boolean remeberOper;
	
	/** 系统运行路径 */
	public static final String runPath;
	
	static
	{
		runPath = new File("").getAbsolutePath().replaceAll("\\\\", "/");
		remeberOper = false;
		
		initUser();
	}
	/** 初始化全局用户 */
	private static void initUser()
	{
		try
		{
			//读取存储的int数组信息
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data/User.data"));
			int chars[] = (int[])ois.readObject();			
			ois.close();
			//获取当前解密方式,如果跟加密方式不一致,将获取不到正确内容
			Class<?> clas = Class.forName(getProperty("加密方式"));
			//获取解密方法
			Method method = clas.getMethod("Decipher",int[].class);
			//执行解密
			int newChars[] = (int[]) method.invoke(clas.newInstance(), chars);
			//解密过的内容
			String nameAndPassWord = Tools.getString(newChars);
			//创建用户
			User temp = new User(nameAndPassWord.split(" ")[0], nameAndPassWord.split(" ")[1]);
			//是否存在,存在就登陆用户
			operuser.exists(temp);
			
		}catch(Throwable e)
		{
			user = new User();
		}
	}
	
}