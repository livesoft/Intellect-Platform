package core.standard;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import core.manage.FileManage;
import core.manage.InputOutputManage;
/**
 * 平台中的配置信息.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
@SuppressWarnings("unchecked")
public class Config extends Object
{
	/** 全局参数的配置. */
	private static Properties properties;

	/** 平台的文件管理模块. */
	protected final static FileManage FM;
	
	/** 平台的输入,输出管理 */
	protected final static InputOutputManage IO;
	
	static
	{
		try
		{
			
			File file = FileManage.createFileManage().getFile("config.ini");
			if(file.exists())
			{
				//初始化配置
				properties = new Properties();
				properties.load(new FileReader(file));
			}else
			{
				initProperties();
			}
		}catch(Exception e)
		{
			InputOutputManage.createIOManage().error("初始化配置文件错误!",e);
		}
		Class<FileManage> fileclass = null;
		try
		{
			fileclass = (Class<FileManage>) Class.forName(getProperty("文件管理"));
		}catch(Throwable e)
		{
			
		}
		if(fileclass != null)
		{
			FM = FileManage.createFileManage(fileclass);
		}else
		{
			FM = FileManage.createFileManage();
		}
		
		Class<InputOutputManage> ioclass = null;
		try
		{
			ioclass = (Class<InputOutputManage>) Class.forName(getProperty("输入输出"));
		}catch(Throwable e)
		{
			
		}
		if(ioclass != null)
		{
			IO = InputOutputManage.createIOManage(ioclass);
		}else
		{
			IO = InputOutputManage.createIOManage();
		}
		
		
	}
	/**
	 * 初始化配置文件.
	 * 如果配置文件不存在,则执行此方法.
	 */
	private static void initProperties()
	{
		properties.setProperty("版本信息", "2.0");
		properties.setProperty("命令路径", "core.commend;ext.commend;");
		properties.setProperty("广播地址", "230.255.48.48");
		properties.setProperty("广播端口", "7039");
		properties.setProperty("连接端口", "7040");
		properties.setProperty("日志文件", "/log/");
		properties.setProperty("广播次数", "0");
		properties.setProperty("广播间隔", "2000");
		properties.setProperty("加密方式", "core.model.datasafe.Simple");
		properties.setProperty("自动执行", "false");
		try
		{
			properties.store(new FileWriter("config.ini"), "Intellect Platform [ version " + getProperty("版本信息") + " ]");
		} catch (Exception e)
		{
			IO.error("配置文件写出错误!",e);
		}
	}
	/**
	 * 获取配置信息.
	 * @param name 配置的key
	 */
	protected static String getProperty(String name)
	{
		return getProperty(name,"");
	}
	/**
	 * 获取配置信息.
	 * @param name 配置的key
	 */
	protected static String getProperty(String name,String defaultStr)
	{
		return properties.getProperty(name,defaultStr);
	}
	protected static void setProperty(String name,String value)
	{
		properties.setProperty(name,value);
	}
	/**
	 * 把配置文件写到文件中!
	 */
	protected static void writeProperties()
	{
		try
		{
			properties.store(new FileWriter("config.ini"), "Intellect Platform [ version " + properties.getProperty("版本信息") + " ]");
		} catch (IOException e)
		{
			System.err.println("配置文件写出失败!");
		}
	}
	/**
	 * @return 平台的文件管理器
	 */
	public static FileManage getFM()
	{
		return FM;
	}
	/**
	 * @return 平台的输出输出
	 */
	public static InputOutputManage getIO()
	{
		return IO;
	}
}
