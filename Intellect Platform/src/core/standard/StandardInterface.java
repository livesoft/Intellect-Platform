package core.standard;

/**
 * 标准的接口. 主要定义一些系统常量.
 * 
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public interface StandardInterface
{
	// 常规设置
	/** 读取时候数组的缓冲 */
	public final short readSize = 2048;

	// 管理方式
	/** 命令行模式. */
	public final byte MODE_COMMEND = 0;
	/** 图形化模式. */
	public final byte MODE_GUI = 1;
	/** 浏览器模式. */
	public final byte MODE_NET = 2;

	// 命令权限
	/** 提供的匿名权限 */
	public final byte POWER_EVERYONE = 4;
	/** 对软件开发者提供的权限 */
	public final byte POWER_SOFTWARE = 3;
	/** 对网络中计算机之间提供的权限 */
	public final byte POWER_NETER = 2;
	/** 对平台管理者提供的权限 */
	public final byte POWER_MANAGER = 1;
	/** 本地的管理员提供的权限 */
	public final byte POWER_SUPER = 0;
	/** 不对任何人提供权限 */
	public final byte POWER_NOBODY = -1;

	// 默认网络连接配置
	/** 默认Socket连接端口 */
	public final int PORT_CONNECT = 7040;
	/** 默认广播端口 */
	public final int PORT_BROADCAST = 7039;
	/** 默认广播地址 */
	public final String ADDRESS_BROADCAST = "230.255.48.48";
}
