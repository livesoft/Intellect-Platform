package core.manage;

import java.io.File;
import core.manager.imple.FileManageJDK;
import core.standard.StandardClass;
/**
 * 文件管理模块.
 * 
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public abstract class FileManage extends StandardClass
{
	/** 
	 * 采用默认JDK的方式创建文件管理系统 
	 * @return 管理器
	 */
	public static FileManage createFileManage()
	{
		return new FileManageJDK();
	}
	/**
	 * 根据指定类型创建文件管理模块.
	 * @param manage 指定的类型
	 * @return 管理器
	 */
	public static FileManage createFileManage(Class<FileManage> manage)
	{
		FileManage FM = null;
		try
		{
			FM = manage.newInstance();
		}catch(Exception e)
		{
			IO.error("初始化文件管理模块出错!", e);
		}
		return FM;
	}
	/**
	 * 获取到文件对象.
	 * 
	 * @param name 要获取的文件名.
	 * @return 文件对象
	 */
	public abstract File getFile(String name);
	/**
	 * 读取一个文件的内容.
	 * 
	 * @param file 要读取的文件.
	 * @return 读取到的字符数组.
	 */
	public abstract byte[] readFile(File file);
	/**
	 * 把内容写到文件中.
	 * @param file  要写的文件 
	 * @param chars 要写内容数组
	 * @param off   所写内容数组下标开始位置
	 * @param n     所写内容数组的长度
	 * @return 是否写成功
	 */
	public abstract boolean writeFile(File file,char[] chars,int off,int n);
	/**
	 * 把内容写到文件中.
	 * 
	 * @param file  要写的文件 
	 * @param bytes 要写内容数组
	 * @param off   所写内容数组下标开始位置
	 * @param n     所写内容数组的长度
	 * @return 是否写成功
	 */
	public abstract boolean writeFile(File file,byte[] bytes,int off,int n);
	/**
	 * 把内容写到文件中.
	 * 
	 * @param file  要写的文件
	 * @param chars 要写的内容
	 * @return 是否写成功
	 */
	public abstract boolean writeFile(File file,char[] chars);
	/**
	 * 把内容写到文件中.
	 * 
	 * @param file  要写的文件
	 * @param bytes 要写的内容
	 * @return 是否写成功
	 */
	public abstract boolean writeFile(File file,byte[] bytes);
}
