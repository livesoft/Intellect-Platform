package core.manage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import core.manager.imple.IO_Print;
import core.standard.StandardClass;
/**
 * 输入/输出管理模块.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public abstract class InputOutputManage extends StandardClass
{
	/** 
	 * 采用默认JDK的方式创建文件管理系统 
	 * @return 默认管理器
	 */
	public static InputOutputManage createIOManage()
	{
		return new IO_Print();
	}
	/**
	 * 根据指定类型创建文件管理模块.
	 * @param manage 指定的类型
	 * @return 指定管理器
	 */
	public static InputOutputManage createIOManage(Class<InputOutputManage> manage)
	{
		InputOutputManage IO2 = null;
		try
		{
			IO2 = manage.newInstance();
		}catch(Exception e)
		{
			IO.error("初始化文件管理模块出错!", e);
		}
		return IO2;
	}
	/**
	 * 输出异常信息.
	 * @param message 异常信息
	 * @param e 异常对象
	 */
	public void error(Object message,Exception e)
	{
		error(message.toString(), e);
	}
	/**
	 * 输出信息
	 */
	public void println()
	{
		print("\r\n");
	}
	/**
	 * 输出信息
	 * @param str 指定的字符串
	 */
	public void println(String str)
	{
		print(str + "\r\n");
	}
	/**
	 * 输出信息
	 * @param cls 指定的类
	 */
	public void println(Class<?> cls)
	{
		print(cls + "\r\n");
	}
	/**
	 * 从流中获取信息
	 * @param is 指定的输入流
	 */
	public synchronized void readInput(InputStream is)
	{
		byte buf[] = new byte[2048];
		int n;
		try
		{
			while((n = is.read(buf)) != -1)
			{
				print(new String(buf,0,n));
			}
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 输出信息
	 * @param message 异常信息
	 * @param e 异常对象
	 */
	public abstract void error(String message,Exception e);
	/**
	 * 输出信息
	 * @param str 指定的字符串
	 */
	public abstract void print(String str);
	/**
	 * 输出信息
	 * @param cls 指定的类.
	 */
	public abstract void print(Class<?> cls);
	/**
	 * 读取一行内容
	 * @return 读取到的内容
	 */
	public abstract String nextLine();
	/**
	 * 读取内容
	 * @return 读取到的内容
	 */
	public abstract String next();
	/**
	 * 读取密码
	 * @return 读取到的密码
	 */
	public abstract String readPassword();
	/**
	 * 从输入流中读取信息.
	 * @param is 指定的输入流
	 * @return 读取到的内容
	 */
	public abstract String readInputStream(InputStream is);
	/**
	 * 把信息写到输出流中.
	 * @param os 指定的输出流
	 * @param str 指定的字符串
	 */
	public abstract void writeOutputStream(OutputStream os,String str);
}
