package core.manager.imple;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import core.manage.FileManage;
/**
 * 采用JDK的方式管理.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class FileManageJDK extends FileManage
{
	/**
	 * 获取到文件对象.
	 * 
	 * @param name 要获取的文件名.
	 * @return 文件对象
	 */
	public File getFile(String name)
	{
		return new File(name);
	}
	/**
	 * 读取一个文件的内容.
	 * 此处采用Java自带的JDK方式读取.
	 * 
	 * @param file 要读取的文件.
	 * @return 读取到的字符数组.
	 */
	public byte[] readFile(File file)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();//缓冲
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			byte c[] = new byte[readSize];
			int n;
			while ((n = fis.read(c)) != -1)
			{
				baos.write(c, 0, n);
			}						
		}catch(Exception e)
		{
			IO.error("读取文件失败!文件<" + file.getAbsolutePath() + ">不存在!",e);
			return null;
		}finally
		{
			//关闭输入流
			if(fis != null)
			{
				try
				{
					fis.close();
				} catch (IOException e)
				{
					IO.error("发生了I/O错误!",e);
					return null;
				}
			}
		}
		byte[] bytes = baos.toByteArray();
		try
		{
			baos.close();
		} catch (IOException e)
		{
			IO.error("发生了I/O错误",e);
			return null;
		}
		return bytes;
	}
	/**
	 * 把内容写到文件中.
	 * @param file  要写的文件 
	 * @param chars 要写内容数组
	 * @param off   所写内容数组下标开始位置
	 * @param n     所写内容数组的长度
	 */
	public boolean writeFile(File file,char[] chars,int off,int n)
	{
		FileWriter fw = null;
		try
		{
			fw = new FileWriter(file);
			fw.write(chars, off, n);			
		}catch(Exception e)
		{
			IO.error("写入文件失败!文件<" + file.getAbsolutePath() + ">不能写入!",e);
			return false;
		}finally
		{
			//关闭输入流
			if(fw != null)
			{
				try
				{
					fw.close();
				} catch (IOException e)
				{
					IO.error("发生了I/O错误!",e);
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 把内容写到文件中.
	 * @param file  要写的文件 
	 * @param bytes 要写内容数组
	 * @param off   所写内容数组下标开始位置
	 * @param n     所写内容数组的长度
	 */
	public boolean writeFile(File file,byte[] bytes,int off,int n)
	{
		FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream(file);
			fos.write(bytes, off, n);			
		}catch(Exception e)
		{
			IO.error("写入文件失败!文件<" + file.getAbsolutePath() + ">不能写入!",e);
			return false;
		}finally
		{
			//关闭输入流
			if(fos != null)
			{
				try
				{
					fos.close();
				} catch (IOException e)
				{
					IO.error("发生了I/O错误!",e);
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 把内容写到文件中.
	 * @param file  要写的文件
	 * @param chars 要写的内容
	 */
	public boolean writeFile(File file,char[] chars)
	{
		return writeFile(file, chars, 0, chars.length);
	}
	/**
	 * 把内容写到文件中.
	 * @param file  要写的文件
	 * @param bytes 要写的内容
	 */
	public boolean writeFile(File file,byte[] bytes)
	{
		return writeFile(file, bytes,0,bytes.length);
	}
//	public static void main(String[] args)
//	{
//		FileManage fm = new FileManageJDK();
//		File file = new File("d:/123.txt");
//		byte bytes[]  = fm.readFile(file);
//		for (int i = 0; i < bytes.length; i++)
//		{
//			System.out.println(bytes[i]);
//		}
//	}
}
