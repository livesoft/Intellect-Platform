package core.manager.imple;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import core.manage.InputOutputManage;
/**
 * 采用JDK的方式使用.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class IO_Print extends InputOutputManage
{
	/** 平台中的标准输入流 */
	private static Scanner scanner = new Scanner(System.in);
	
	/**
	 * 平台的错误输出.
	 */
	public void error(String obj,Exception e)
	{
		if(getProperty("输出错误").equals("true"))
		{
			System.err.println(obj);
			e.printStackTrace();
		}
	}
	/**
	 * 平台的标准输出.
	 */
	public void print(String obj)
	{
		if(mangerModel == MODE_COMMEND)
		{
			System.out.print(obj);
		}else
		{
			area.append(obj);
		}
		
	}
	public void print(Class<?> cls)
	{
		print(cls.toString());
	}
	public String nextLine()
	{
		return scanner.nextLine();
	}
	public String next()
	{
		return scanner.next();
	}
	public String readPassword()
	{
		return new String(System.console().readPassword(""));
	}
	public String readInputStream(InputStream is)
	{
		StringBuilder builder = new StringBuilder();
		DataInputStream dis = new DataInputStream(is);
		byte buf[] = new byte[2048];
		int n;
		try
		{
			while((n = dis.read(buf)) != -1)
			{
				builder.append(new String(buf,0,n));
			}
		}catch(Exception e)
		{
			error("读取错误", e);
		}
		return builder.toString();
	}
	public void writeOutputStream(OutputStream os, String str)
	{
		DataOutputStream dos = new DataOutputStream(os);
		try
		{
			dos.write(str.getBytes());
		} catch (IOException e)
		{
			error("写入错误", e);
		}
	}
	
}
