package core.bin.console;

import java.io.File;
import java.util.Date;
import java.util.Scanner;
import core.bin.fream.Fream;
import core.model.commend.log;
import core.model.commend.reboot;
import core.model.thread.InterpreterThread;
import core.model.thread.TimerThread;

/**
 * 为平台提供一个良好的控制台.
 * 
 * @since JDK 1.6
 * @version 1.0
 */
public final class Console extends Other
{

	/** 重启时候的参数,windows.bat等. */
	private static String Rebootarg; 
	/** 初始化平台中的一些内容. */
	static
	{
		//初始化操作时间
		date = new Date();
	}
	/** 
	 * 建立命令控制台. 
	 * @param reboot 重启参数
	 */
	public Console(String reboot)
	{
		Rebootarg = reboot;
		Thread mainThread = new Thread(new InterpreterThread(this));
		Thread timeThread = new Thread(new TimerThread());
		timeThread.setDaemon(true);
		mainThread.start();
		timeThread.start();
	}
	/**
	 * 连接管理平台
	 */
	public void onLine()
	{
		//记录连接
		log.takeNotes(user, "连接成功");
		//回显版本
		System.out.println(copyAllright());
		while(true)
		{
			
			//回显提示符
			System.out.print("[" + user + "]→");
			//读取命令  BUG nextLine报错 不会阻塞 
			//因为System.in 流已经关闭 所以会报错
			String input = IO.nextLine();
			if(remeberOper)
			{
				//记录命令
				appendOper(input);
			}
			//重新记录时间
			date = new Date();
			if(input.equals("reboot"))
			{
				log.takeNotes(user, "reboot");
				reboot.start(Rebootarg);
				exit();
				break;
			}else if(input.equals("fream"))
			{
				new Fream();
				mangerModel = MODE_GUI;
				break;
			}
			IO.print(exec(input));//执行命令
		}
	}
	/**
	 * 自动执行命令.
	 */
	public void auto_commend()
	{
		try
		{
			//记录的单条记录
			Scanner sc = new Scanner(new File(operFile));
			String input;
			do
			{
				input = sc.nextLine();
				//截取 本命令距离下次命令的时间
				String time = input.substring(input.indexOf('[') + 1, input.indexOf(']'));
				//截取 相应的命令
				String commend = input.substring(input.indexOf(']') + 1);
				//休眠
				Thread.sleep(Long.parseLong(time));
				//执行并回显
				IO.println("[ " + user.getUserName() + " 自动化执行 ]→" + commend);
				exec(commend);
			}while(input != null);
			sc.close();
		} catch (Exception e)
		{
			 
		}
		IO.println();
		IO.println("[ " + user.getUserName() + " 自动化执行 ]→执行完毕,断开连接!");
		exit();
	}

}
