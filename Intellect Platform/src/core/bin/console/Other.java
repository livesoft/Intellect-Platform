package core.bin.console;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.Date;
import core.bin.fream.LoginFream;
import core.bin.phone.TCP_PCServer;
import core.model.commend.log;
import core.model.commend.operuser;
import core.model.commend.path;
import core.model.entity.User;
import core.standard.StandardClass;

/**
 * 管理平台的辅助方法.
 * 
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class Other extends StandardClass
{	
	private static String allCommend = "";
	/** 当前记录操作的个数 */
	private static int count = 0;
	/** 操作记录的文件. */
	protected static String operFile = "data/Oper.data";
	
	/** 上一次操作的时间. */
	protected static Date date;
	/** 展示在控制台头部的信息. */
	protected static String copyAllright()
	{
		String info = "Intellect Platform [版本 " + getProperty("版本信息") + "]";
		info = info + "\r\n智能平台 李瑜 版权所有\r\n如果你发现BUG或者有更好的建议!";
		info = info + "\r\n请联系我:346886697@qq.com";
		info = info + "\r\n如果初次使用,建议使用fream命令启动图形化管理.";
		return info;
	}
	/** 显示常用的帮助命令 */
	protected static String help()
    {
    	return 
		("常用帮助命令:") 											         + "\r\n" +
		("exit     退出管理平台") 									         + "\r\n" +
		("fream    打开图形化管理(手机端无法执行)") 					     + "\r\n" +
		("phone    打开手机端服务") 							             + "\r\n" +
		("login    使用用户登录到平台,如果用户名输入\"everyone\"将切换到匿名") + "\r\n" +
		("reboot   重启管理平台") 									         + "\r\n" +
		("path     当前用户的环境变量")                             	     + "\r\n" +
		("log      日志相关的一些操作") 							         + "\r\n" +
		("operuser 关于用户操作的命令组")								     + "\r\n" +
		("osinfo   关于系统信息的一些操作") 							     + "\r\n" +
		("udp      广播相关的操作") 								         + "\r\n" +
		("connect  连接相关的操作") 								   		 + "\r\n" +
		("task     任务相关的操作") 								  	     + "\r\n" +
		("error    错误相关的操作") 								    	 + "\r\n" +
		("remeber  自动化相关的操作") 							        	 + "\r\n" +
		("\r\n直接输入命令 可以查看命令的使用方式")                       	 + "\r\n" ;
    }
    /** 登录到管理平台 */
	protected static void login()
	{
		if(mangerModel == MODE_GUI)
		{
			new LoginFream();
		}else
		{
			
			IO.print("请输入用户名:");
			String userName = IO.next();
			if(userName.toLowerCase().equals("everyone"))
			{
				user = new User();
				IO.println("切换到匿名登录!");
			}else
			{
				IO.print("请输入密码:");
							
				String passWord = "";
				try
				{
					//以下只能在原生控制台 暗文
					passWord = IO.readPassword();
				}catch(Exception e)
				{
					//以下可以在任何地方使用 明文
					passWord = IO.next();
				}
				User temp = new User(userName, passWord);//创建临时用户
				if(operuser.exists(temp) != -1)
				{
					//是否强制更改密码
					if(user.isMustAlter())
					{
						IO.println("您的账户需要更改密码!");
						user.setMustAlter(false);
						//修改密码
						operuser.changepassword();
					}
					log.takeNotes(user, "连接成功");
					IO.println("用户登录成功!");
					
					return; 
				}
				IO.println("登录失败!用户名或密码错误!");
			}	
			
		}
	}
	/** 结束管理平台. */
	protected static void exit()
	{
		IO.println("释放占用的资源");
		Runtime.getRuntime().gc();
		IO.println("等待结束主线程!");
		System.exit(0);
	}
	/** 启动手机端服务器 */
	protected static String phone()
	{
		new Thread(new TCP_PCServer()).start();
		try
		{
			Thread.sleep(500);
		} catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		return "";
	}
	/**
	 * 把命令写入到文件中.
	 * @param commend 要写入的命令
	 */
	protected static void appendOper(String commend)
	{
		//记录命令的文件
		File file = new File("data/Oper.data");
		
		//退出命令 不写到文件中,但是当前其他信息要写到文件中
		if(commend.equals("exit"))
		{
			writeCommendToFile(file);
			return;
		}
		//命令的时间
		String time = new Date().getTime() - date.getTime() + "";
		if(count == 0)
		{
			time = "0";
		}
		//存储的内容
		allCommend = allCommend +  "[" + time + "]" + commend + "\r\n";
		count++;
		if(count % 8 == 0)
		{
			writeCommendToFile(file);
		}
	}
	/**
	 * 将当前记录的命令写到文件中.
	 * @param file 要写的文件
	 */
	private static String writeCommendToFile(File file)
	{
		try
		{
			FileOutputStream fos = new FileOutputStream(file,true);
			fos.write(allCommend.getBytes());
			fos.close();
			allCommend = "";
			return "命令已存储\r\n";
		} catch (Exception e)
		{
			
		}
		return "命令未储存\r\n";
	}
	/**
	 * 在管理平台上执行命令.
	 * @param input 输入的一整串命令
	 * 
	 */
	protected static String exec(String input)
	{
		//根据输入的信息,获取到命令类
		String str[] = input.trim().split(" ");
		String commend = str[0];
		if(!commend.equals(""))
		{
			//记录日志
			log.takeNotes(user, input);
			//命令类的默认方法为 help
			String info = str.length > 1 ? str[1] : "help";
			//分解命令中的参数
			String args = "";
			for (int i = 2; i < str.length; i++)
			{
				args = args + str[i] + " ";
			}

			if(commend.equals("exit"))
			{
				//退出命令
				exit();
			}else if(commend.equals("login"))
			{
				//命令提示符登录命令
				login();
			}else if(commend.equals("help"))
			{
				//帮助命令
				return help();
			}else if(commend.equals("phone"))
			{
				//帮助命令
				return phone();
			}else
			{
				return exec(commend, info, args);	
			}
		}
		return "";
	}
	/**
	 * 执行命令.
	 * @param commend 执行命令所在的类
	 * @param info    执行命令
	 * @param args    执行命令所需的参数
	 */
	private static String exec(String commend,String info,String args)
	{
		try
		{
			//加载命令所在的类
			Class<?> commendClass = path.findCommend(commend);
			if(commendClass != null)
			{
				//得到命令的权限
				byte type = (Byte) commendClass.getMethod("getPower").invoke(commendClass.newInstance());
				//byte type = (Byte) Tools.runMethod(commendClass, "getPower");
				if(type < user.getLevel())
				{
					return "没有权限执行\r\n";
				}else
				{
					Object obj = null;
					if(args.equals(""))
					{
						//无参方法
						Method method = commendClass.getMethod(info);
						obj = method.invoke(commendClass.newInstance());
					}else
					{
						//指定参数
						Method method = commendClass.getMethod(info,String.class);
						obj = method.invoke(commendClass.newInstance(),args);
					}
					if(obj != null && obj.getClass().equals(String.class))
					{
						return obj.toString();
					}else
					{
						return "";
					}
				}
			}else
			{
				//命令类不存在
				return "没有找到命令组: " + commend + "\r\n";
			}
		}catch(NoSuchMethodException nse)
		{
			//命令类存在,二级方法不存在
			return "没有找到命令: " + commend + " " + info + "\r\n";
		}catch(Exception e)
		{
			//e.printStackTrace();
			//命令运行过程出现错误 可以使用error调用出原因
			return "命令运行错误:" + commend + " 错误原因: " + e.getMessage() + "\r\n";
		}catch(Error e) 
		{
			return "系统内部发生错误!" + " " + e.getMessage() + "\r\n";
		}
	}
}
