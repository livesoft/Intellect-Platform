package core.bin.phone;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import core.standard.StandardClass;
/**
 * 主要负责电脑跟手机的连接.
 * @version 2.0
 * @since JDK1.7
 * @author 李志刚
 */
public class TCP_PCServer extends StandardClass implements Runnable
{
	private boolean state = true;//线程状态
	
	/**
	 * 监听地址
	 */
	public static final String SERVERIP = "127.0.0.1";

	/**
	 * 监听端口
	 */
	public static final int SERVERPORT = 1818;

	private ArrayList<SocketThread> lists = new ArrayList<>();//所有socket连接
	
	public void run()
	{
		ServerSocket serverSocket = null;
		try
		{
			serverSocket = new ServerSocket(SERVERPORT);
			IO.println("手机服务器已启动");
		} catch (Exception e1)
		{
			IO.println("手机服务器已启动!无需再次启动!");
			return;
		}
		while (state)
		{
			try
			{
				Socket client = serverSocket.accept();
				IO.println("手机客户:" + client.getInetAddress() + "已连接!");
				SocketThread st = new SocketThread(client);
				new Thread(st).start();
				lists.add(st);
			} catch (IOException e1)
			{
				IO.println("等待客户连接");
			}
		}
		if (serverSocket != null)//关闭连接
		{
			try
			{
				serverSocket.close();
			} catch (IOException e)
			{

			}
		}
	}
	/** 停止服务器 */
	public void stop()
	{
		state = false;
	}
//	public static void main(String a[])
//	{
//
//		Thread desktopServerThread = new Thread(new TCPDesktopServer());
//		desktopServerThread.start();
//	}
//	
}
