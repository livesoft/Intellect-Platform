package core.bin.phone;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import core.bin.console.Other;
import core.model.entity.Message;
import core.model.entity.Task;
/**
 * 主要负责电脑读取手机的内容.
 * @version 2.0
 * @since JDK1.7
 * @author 李志刚
 */
public class SocketThread extends Other implements Runnable
{
	private boolean state = true;
	
	private Socket socket;
	
	/**
	 * 根据socket建立连接线程.
	 * @param socket 对应的连接
	 */
	public SocketThread(Socket socket)
	{
		this.socket = socket;
	}
	public void run()
	{
		ObjectInputStream ois = null;
		while(state)
		{
			String result = "";
			try
			{
				ois = new ObjectInputStream(socket.getInputStream());
				
				@SuppressWarnings("unchecked")
				Map<String,String> map = (Map<String,String>)ois.readObject();
				Message mes = new Message(map);
				Task task = mes.getTask();
				if(task != null && task.getType() == Task.EXECUTE_COMMEND)
				{
					result = "\r\n命令执行结果:\r\n" + exec(task.getStr());
				}
			} catch (Exception e)
			{
				state = false;
				IO.println(e.getMessage());
			}
			try
			{
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				Map<String, String> map = new HashMap<>();
				map.put("name", "服务器端");
				map.put("info", "收到了你的消息!" + result);
				oos.writeObject(map);
				oos.flush();
			}catch(Exception e)
			{
				state = false;
			}
		}
		if(ois!=null)
		{
			try
			{
				ois.close();
			} catch (IOException e)
			{
				
			}
		}
	}
}
