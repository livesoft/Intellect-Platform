package core.bin;

import core.bin.console.Console;
import core.standard.StandardClass;
/**
 * 平台的入口.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class Run extends StandardClass
{
	/**
	 * @param args 运行时参数
	 */
	public static void main(String[] args)
	{
		String Rebootarg = "";
		if (args.length > 0)
		{
			Rebootarg = args[0];
		}
		CheckThread();
		new Console(Rebootarg);
	}
	/**
	 * 暂时不用.
	 */
	public static void CheckThread()
	{
//		if(broadcastThread == null)
//		{
//			IO.println("广播线程未打开,可以使用udp build命令开启!");
//		}
//		if(serverSocketThread == null)
//		{
//			IO.println("后台监听服务未开启,可以使用connect server命令开启!");
//		}
	}
}
