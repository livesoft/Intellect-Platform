package core.bin.fream;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import core.standard.StandardClass;
/**
 * 窗体中按钮的事件.
 * @version 2.0
 * @since JDK1.7
 * @author 李瑜
 */
public class ButtonListern extends StandardClass implements ActionListener
{

	public void actionPerformed(ActionEvent e)
	{
		JButton btn = (JButton)e.getSource();
		switch (btn.getText())
		{
			case "清除":
				area.setText("");
				break;
		}
	}
	
}
