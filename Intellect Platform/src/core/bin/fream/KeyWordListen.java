package core.bin.fream;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JTextField;
import core.bin.console.Other;
/**
 * 键盘事件.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class KeyWordListen extends Other implements KeyListener
{
	private JTextField field;
	/**
	 * @param field 指定的输入框
	 */
	public KeyWordListen(JTextField field)
	{
		this.field = field;
	}
	public void keyTyped(KeyEvent e){}
	public void keyPressed(KeyEvent e)
	{
		//将来做提示使用
	}
	public void keyReleased(KeyEvent e)
	{
		int keyCode = e.getKeyCode();
		switch (keyCode)
		{
			case 10:
				String input = field.getText();
				field.setText("");
				area.append(exec(input));
				break;

			default:
				break;
		}
	}
}
