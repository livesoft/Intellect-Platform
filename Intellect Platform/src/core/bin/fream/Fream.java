package core.bin.fream;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.File;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import core.bin.console.Other;
/**
 * 图形化管理方式
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class Fream extends Other implements WindowFocusListener
{
	protected JMenuBar menuBar;
	protected JFrame frame;//窗体
	protected JComponent allComponent[] = new JComponent[20];//本窗体中全部的组件
	/*
	 * 布局文件:
	 * 输入框
	 * 文本区域
	 * 标签
	 * 服务器按钮 8个
	 * 节点端按钮 8个
	 * 清除按钮 
	 */
	protected JPanel serverPanel,clientPanel;
//	public static void main(String[] args)
//	{
//		mangerModel = MODE_GUI;
//		new Fream();
//	}
	/**
	 * 默认构造窗体.
	 */
	public Fream()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			IO.println("初始化窗体");
			init_JComponent();
			init_JFream();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * 初始化标签.
	 */
	private void init_JComponent() throws Exception
	{
		serverPanel = new JPanel(null);
		serverPanel.setBackground(Color.WHITE);
		clientPanel = new JPanel(null);
		clientPanel.setBackground(Color.WHITE);
		
		//输入框
		allComponent[0] = new JTextField();
		allComponent[0].setFont(new Font("", 1, 22));
		allComponent[0].addKeyListener(new KeyWordListen((JTextField)allComponent[0]));
		
		//清除按钮
		allComponent[19] = new JButton("清除");
		((JButton)allComponent[19]).addActionListener(new ButtonListern());
		allComponent[19].setFont(new Font("", 0, 18));
		
		//文本区域
		area.setEditable(false);
		area.setText(copyAllright() + "\r\n");
		area.setFont(new Font("",0,18));
		allComponent[1] = new JScrollPane(area);
		allComponent[1].setFont(new Font("",0,20));
		
		allComponent[2] = new JTabbedPane(1);
		allComponent[2].add(serverPanel,"服务器");
		allComponent[2].add(clientPanel,"节点端");
		allComponent[2].setFont(new Font("", 0, 18));
		
		//服务器
		allComponent[3]  = new JButton("建立广播");
		allComponent[4]  = new JButton("停止广播");
		allComponent[5]  = new JButton("建立连接");
		allComponent[6]  = new JButton("断开连接");
		allComponent[7]  = new JButton("发布任务");
		allComponent[8]  = new JButton("取消任务");
		allComponent[9]  = new JButton("查看状态");
		allComponent[10] = new JButton("查看连接");
		for (int i = 3; i <= 10; i++)
		{
			serverPanel.add(allComponent[i]);
			allComponent[i].setEnabled(false);
			((JButton)allComponent[i]).addActionListener(new ButtonListern());
		}
		
		
		//客户端
		allComponent[11] = new JButton("接受广播");
		allComponent[12] = new JButton("停止接受");
		allComponent[13] = new JButton("建立连接");
		allComponent[14] = new JButton("断开连接");
		allComponent[15] = new JButton("接收任务");
		allComponent[16] = new JButton("停止任务");
		allComponent[17] = new JButton("查看状态");
		allComponent[18] = new JButton("查看连接");
		for (int i = 11; i <= 18; i++)
		{
			clientPanel.add(allComponent[i]);
			allComponent[i].setEnabled(false);
			((JButton)allComponent[i]).addActionListener(new ButtonListern());
		}
		
		//设置布局
		File file = new File("data/Fream.data");
		Scanner sc = new Scanner(file);
		for (int i = 0; i < allComponent.length; i++)
		{
			int nums[] = split(sc.nextLine());
			allComponent[i].setBounds(nums[0],nums[1],nums[2],nums[3]);
		}
		sc.close();
		menuBar = new JMenuBar();
		
		JMenuItem menu[][] = new JMenuItem[4][];
		
		menu[0] = new JMenuItem[4];
		menu[0][0] = new JMenu("常规");
		menu[0][1] = new JMenuItem("记住命令");
		menu[0][2] = new JMenuItem("记住用户");
		menu[0][3] = new JMenuItem("退出程序");	
		
		menu[1] = new JMenuItem[4];
		menu[1][0] = new JMenu("命令");
		menu[1][1] = new JMenuItem("帮助");	
		menu[1][2] = new JMenuItem("手机");
		menu[1][3] = new JMenuItem("修改平台");
		
		menu[2] = new JMenuItem[3];
		menu[2][0] = new JMenu("用户");
		menu[2][1] = new JMenuItem("登陆");
		menu[2][2] = new JMenuItem("退出");
		
		menu[3] = new JMenuItem[4];
		menu[3][0] = new JMenu("连接");
		menu[3][1] = new JMenuItem("成为服务器");
		menu[3][2] = new JMenuItem("成为节点端");
		menu[3][3] = new JMenuItem("连接状态");
		
		//添加菜单
		for (int i = 0; i < menu.length; i++)
		{
			for (int j = 1; j < menu[i].length; j++)
			{
				menu[i][0].add(menu[i][j]);
			}
			menuBar.add(menu[i][0]);
		}
		
		//给菜单添加事件
		for (int i = 0; i < menu.length; i++)
		{
			for (int j = 0; j < menu[i].length; j++)
			{
				JMenuItem item = menu[i][j];
				item.addActionListener(new MenuListern());
			}
		}
		
	}
	/**
	 * 初始化窗体
	 */
	private void init_JFream() throws Exception
	{
		while(user == null)
		{
			IO.println("等待加载信息...");
			Thread.sleep(1000);
		}
		frame = new JFrame("智能平台 - 管理模式 - " + user.getUserName());
		frame.setJMenuBar(menuBar);
		Container container = frame.getContentPane();
		container.setLayout(null);
		container.add(allComponent[0]);
		container.add(allComponent[1]);
		container.add(allComponent[2]);
		container.add(allComponent[19]);
		
		Toolkit toolkit = frame.getToolkit();
		Dimension dimension = toolkit.getScreenSize();
		frame.addWindowFocusListener(this);
		frame.setIconImage(toolkit.createImage("ico.png"));
		frame.setBounds(dimension.width / 2 - 340, dimension.height / 2 - 220, 680, 440);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	private static int[] split(String info)
	{
		info = info.substring(1, info.length() - 1);
		String infos[] = info.split(",");
		if(infos.length == 4)
		{
			int nums[] = new int[4];
			nums[0] = Integer.parseInt(infos[0]);
			nums[1] = Integer.parseInt(infos[1]);
			nums[2] = Integer.parseInt(infos[2]);
			nums[3] = Integer.parseInt(infos[3]);
			return nums;
		}
		return null;
	}
	
	/**
	 * 刷新标题
	 */
	public void flush()
	{
		//System.out.println("智能平台 - 管理模式 - " + user.getUserName());
		frame.setTitle("智能平台 - 管理模式 - " + user.getUserName());
	}
	public void windowGainedFocus(WindowEvent e)
	{
		flush();
	}
	public void windowLostFocus(WindowEvent e)
	{
	}
	
}
