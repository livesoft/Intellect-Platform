package core.bin.fream;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import core.bin.console.Other;
import core.model.commend.connect;
import core.model.commend.remeber;
import core.model.entity.User;
/**
 * 菜单事件.
 * @version 1.0
 * @since JDK1.7
 * @author 李瑜
 */
public class MenuListern extends Other implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		JMenuItem item = (JMenuItem)e.getSource();
		switch (item.getText())
		{
			case "退出程序":
				System.exit(0);
				break;
			case "记住命令":
				remeber.oper();
				break;
			case "记住用户":
				remeber.user();
				item.setText("忘记用户");
				break;
			case "忘记用户":
				remeber.unuser();
				item.setText("记住用户");
				break;
			case "修改平台":
				JOptionPane.showMessageDialog(null, "修改模块已开启,具体修改方式请查看帮助文档!");
				break;
			case "登陆":
				new LoginFream();
				break;
			case "手机":
				phone();
				break;
			case "帮助":
				IO.println(help());
				break;
			case "退出":
				user = new User();
				break;
			case "成为服务器":
				new connect().server();
				break;
			case "成为节点端":
				new connect().client();
				break;
			case "连接状态":
				area.append(new connect().state() + "\r\n");
				break;
			default:
				break;
		}
	}
}
